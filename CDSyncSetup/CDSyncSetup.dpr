program CDSyncSetup;

{$R *.dres}

uses
  Forms,
  Windows,
  Dialogs,
  ComObj,
  SysUtils,
  pwd_dlg in 'pwd_dlg.pas' {PasswordDlg},
  main in 'main.pas' {frmRepl},
  fileswork in '..\Modules\fileswork.pas',
  WININFO in '..\Modules\WININFO.pas',
  utils in '..\Modules\utils.pas',
  log in '..\Modules\log.pas',
  optimize in '..\Modules\optimize.pas',
  mutex in '..\Modules\mutex.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfrmRepl, frmRepl);
  if (ParamCount=2) and (LowerCase(ParamStr(2))='rebuild') then
    begin
      CreateRegKey(CustomReg, 'path',ParamStr(1), HKEY_CURRENT_USER);
      frmRepl.ReadConnectionSettings;
      frmRepl.btnCreateTriggersClick(nil);
    end
  else
//  Application.CreateForm(TPasswordDlg, PasswordDlg);
    Application.Run;
end.
