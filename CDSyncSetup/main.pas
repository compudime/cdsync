unit main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, Dialogs, DB, StdCtrls, ShellApi, ComObj,
  ExtCtrls, Generics.Collections, Vcl.Grids, Vcl.DBGrids, Vcl.CheckLst, Vcl.DBCtrls, Vcl.Menus, Vcl.Buttons,
  CheckComboBox, adscnnct, adsdata, adsfunc, adstable, System.Types,
  Vcl.WinXCtrls, RegularExpressions;


type
  Trigger = record
    TriggerType: string;
    Keys: string;
    Columns: string;
    TriggerCode: string;
    TriggerFieldPrefix: string;
  end;


type
  TfrmRepl = class(TForm)
    qryExec: TAdsQuery;
    con: TAdsConnection;
    pnlBottom: TPanel;
    Panel2: TPanel;
    Panel1: TPanel;
    grpDbConnectionSet: TGroupBox;
    edCon: TEdit;
    btnSetCon: TButton;
    oDlg: TOpenDialog;
    dsTabs: TDataSource;
    qryTabs: TAdsQuery;
    mLog: TMemo;
    btnCreateTriggers: TButton;
    Panel3: TPanel;
    GroupBox3: TGroupBox;
    trgRG: TRadioGroup;
    trgSource: TMemo;
    Panel4: TPanel;
    btnTrigSave: TBitBtn;
    dlgSave: TSaveDialog;
    gbtabs: TGroupBox;
    grTabs: TDBGrid;
    Panel6: TPanel;
    Label1: TLabel;
    edFltTab: TEdit;
    btnRefreshTabs: TBitBtn;
    Splitter1: TSplitter;
    qryTabsTable: TAdsStringField;
    qryTabsi_s: TWideMemoField;
    qryTabsu_s: TWideMemoField;
    qryTabsd_s: TWideMemoField;
    qryTabstable_name: TAdsStringField;
    qryTabskeys: TAdsStringField;
    qryTabsins: TIntegerField;
    qryTabsins_exc: TIntegerField;
    qryTabsins_fields: TAdsStringField;
    qryTabsupd: TIntegerField;
    qryTabsupd_exc: TIntegerField;
    qryTabsupd_fields: TAdsStringField;
    qryTabsdel: TIntegerField;
    qryTabsdel_exc: TIntegerField;
    qryTabsdel_fields: TAdsStringField;
    qryTabsExist: TIntegerField;
    tglswtchNewOld: TToggleSwitch;
    Panel5: TPanel;
    mmoCustomTrigger: TMemo;
    mmoKeys: TMemo;
    lblTriggerType: TLabel;
    pnl2: TPanel;
    mmoColumns: TMemo;
    btnSaveTriggerToFile: TButton;
    lblWhereKeys: TLabel;
    lblColumns: TLabel;
    pnlBottomBar: TPanel;
    btnClosePanel: TButton;
    btnSaveKeys: TButton;
    btnSaveColumns: TButton;
    procedure btnClosePanelClick(Sender: TObject);
    procedure btnCreateTriggersClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnSetConClick(Sender: TObject);
    procedure conAfterConnect(Sender: TObject);
    procedure grTabsCellClick(Column: TColumn);
    procedure edFltTabKeyPress(Sender: TObject; var Key: Char);
    procedure dsTabsUpdateData(Sender: TObject);
    procedure edFltTabChange(Sender: TObject);
    procedure trgRGClick(Sender: TObject);
    procedure dsTabsDataChange(Sender: TObject; Field: TField);
    procedure btnTrigSaveClick(Sender: TObject);
    procedure grTabsDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure grTabsColExit(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure cmbDropDown(Sender: TObject);
    procedure cmbCloseUp(Sender: TObject);
    procedure edFltTabEnter(Sender: TObject);
    procedure btnRefreshTabsClick(Sender: TObject);
    procedure btnSaveColumnsClick(Sender: TObject);
    procedure btnSaveKeysClick(Sender: TObject);
    procedure btnSaveTriggerToFileClick(Sender: TObject);
    procedure conBeforeConnect(Sender: TObject);
    procedure tglswtchNewOldClick(Sender: TObject);
  private
    mainSql: string;
    cb: TCheckComboBox;
    tmpLst: TStringList;
    lstCols: TStringList;
    lastModField: string;
    DefaultValueByType : TDictionary<Integer, String>;
    UpdateStatementFormats : TDictionary<Integer, String>;
    CurrentTrigger : Trigger;
    EditedTriggers : TDictionary<String, Boolean>;
    function UserPass: string;
    procedure PopulateDefaultValuesDict(Dict : TDictionary<Integer, String>);
    function GenerateUpdateStatements(ColumnData: TDataSet): String;
    function GetIncludedColumns(TableName, ColumnsToInclude: String;
      Query: TAdsQuery): TDataSet;
    function GetStatement(ColumnName: String; FieldType: Integer): String;
    procedure PopulateUpdateStatmentsFormatDict(
      Dict: TDictionary<Integer, String>);
    procedure ShowTab(Column: TColumn);
    function OverwriteCurrentTrigger(msg : String = '') : Boolean;
    function ExtractFieldsFromTrigger(TriggerCode: String) : TArray<string>;
    procedure SetCustomTriggerPanel(Trig: Trigger);
    procedure ShowCustomTriggerPanel(TriggerText : String);
    function GetCustomTriggerTextOutput: AnsiString;
    procedure SaveTriggerHistoryForTable(TableName: String);
  public
    procedure ReadConnectionSettings;
    function SetNewConnection(AInitialDir: string = ''): string;
    function ExecSql(Sql: string; SepCommandsDelimiter: Char = #0): variant;
    function GetSQLValue(SQLText: string; Delim4Fields: string = ';'; Delim4Recs: string = #13#10): string;
    procedure create_trig(sType: string = '');
    procedure CreateTriggers;
    procedure DropTriggers;
    procedure Upd_Trig_Source_Panel;
    // procedure upd_tab_cols_list;
    function get_cols(sExclCols: string = ''): string;
    procedure upd_sync_settings(fldName: string = ''; Val: string = '');
    procedure init;
    function ExcludeStrs(sLst, sLstExclude: string): string;
  end;

  TGridCracker = class(TCuStomGrid);

function IsDelphiInstalled: Boolean;

var
  frmRepl: TfrmRepl;
  sSyncSql: string;
  sSyncExecSqlRet: variant;

const
  // used for i=insert, u=update, d=delete text
  trig_abbr: array[0..2] of string = ('i', 'u', 'd');
  trig_oper: array[0..2] of string = ('insert', 'update', 'delete');
  tab_names: array[0..2] of string = ('ins', 'upd', 'del');
  CustomReg = 'Software\CDSync';

implementation

uses
  FilesWork, WinInfo, Utils, DateUtils, Log, StrUtils, Registry, FileCtrl,
  AdsSet, System.IniFiles, pwd_dlg;

{$R *.dfm}

function IsDelphiInstalled: Boolean;
const
  DelphiRegistryKeys: array[0..12] of string = ('Software\Borland\Delphi\',    // Delphi 1 to 7
    'Software\CodeGear\BDS\',      // Delphi 2006 to 2010
    'Software\Embarcadero\BDS\',   // Delphi XE to XE8
    'Software\Embarcadero\Studio\', // Delphi 10.0 to 10.4
    'Software\Embarcadero\RAD Studio\21.0\', // Delphi 11 Alexandria,
    'Software\Embarcadero\RAD Studio\22.0\', // Delphi 11 Alexandria
    'Software\Borland\C++Builder\',
    'Software\CodeGear\C++Builder\',
    'Software\Embarcadero\C++Builder\',
    'Software\Embarcadero\C++Builder\Studio\',
    'Software\Borland\Delphi\1.0',
    'Software\Borland\Delphi\2.0',
    'Software\Borland\Delphi\3.0');
var
  Reg: TRegistry;
  i: Integer;
begin
  Result := False;
  Reg := TRegistry.Create(KEY_READ);
  try
    Reg.RootKey := HKEY_CURRENT_USER; // or HKEY_LOCAL_MACHINE, depending on where the Delphi is installed
    for i := Low(DelphiRegistryKeys) to High(DelphiRegistryKeys) do
    begin
      if Reg.KeyExists(DelphiRegistryKeys[i]) then
      begin
        Result := True;
        Exit;
      end;
    end;
  finally
    Reg.Free;
  end;
end;

function DDCryPas: string;
var
  a, b, c: string;
begin
  Result := '$';
  b := 'Encryption';
  a := 'tiswin';
  c := '$';
  Result := Result + UpperCase(Copy(a, 1, 1)) + Copy(a, 2, 2);
  Result := Result + UpperCase(Copy(a, 4, 1)) + Copy(a, 5, 2) + '3' + c;
  c := '%';
  Result := Result + Copy(b, 1, 6) + UpperCase(Copy(b, 7, 4));
  Result := Result + c;
end;

function SetGlobalEnvironment(const Name, Value: string; const User: Boolean): Boolean;
resourcestring
  REG_MACHINE_LOCATION = 'System\CurrentControlSet\Control\Session Manager\Environment';
  REG_USER_LOCATION = 'Environment';
begin
  with TRegistry.Create do
    try
      if User then { User Environment Variable }
        Result := OpenKey(REG_USER_LOCATION, True)
      else { System Environment Variable }
      begin
        RootKey := HKEY_LOCAL_MACHINE;
        Result := OpenKey(REG_MACHINE_LOCATION, True);
      end;
      if Result then
      begin
        { Write Registry for Global Environment }
        WriteString(Name, Value);
        { Update Current Process Environment Variable }
        SetEnvironmentVariable(PChar(Name), PChar(Value));
        { Send Message To All Top Window for Refresh }
        (* DP - this is potentionally dangerous, despite it has know issues. If it is REALLY necessary to broadcast change notification,
          there should be another way. On 2 of my systems this causes program to "hang". So I commented it out for a moment, it is even possible
          that it causes your system to sop working after some time!
        *)
        // SendMessage(HWND_BROADCAST, WM_SETTINGCHANGE, 0, Integer
        // (PChar('Environment')));
      end;
    finally
      Free;
    end;
end; { SetGlobalEnvironment }

function ReadTemplateRes(fName: string; const ReplArr: array of variant): string;
var
  rs: TResourceStream;
  st: TStringList;
begin
  fName := UpperCase(fName);
  // fName := UpperCase('upd_where_keys_sql');
  rs := TResourceStream.Create(HInstance, fName, RT_RCDATA);
  st := TStringList.Create;
  try
    st.LoadFromStream(rs);
    Result := ReplaceAll(st.Text, ReplArr);
  finally
    rs.Free;
    st.Free;
  end;
end;

procedure TfrmRepl.btnClosePanelClick(Sender: TObject);
begin
  Panel5.Visible := False;
  Panel5.SendToBack;
  CurrentTrigger.TriggerType := '';
  CurrentTrigger.TriggerCode := '';
  CurrentTrigger.Keys := '';
  CurrentTrigger.Columns := '';
  CurrentTrigger.TriggerFieldPrefix := '';
end;

procedure TfrmRepl.FormCreate(Sender: TObject);
begin
  tmpLst := TStringList.Create;
  lstCols := TStringList.Create;
  mainSql := qryTabs.Sql.Text;
  EditedTriggers := TDictionary<String,Boolean>.create;
  cb := TCheckComboBox.Create(Self);
  cb.Parent := Self;
  cb.Visible := false;
  cb.OnDropDown := cmbDropDown;
  cb.OnCloseUp := cmbCloseUp;
  lastModField := '';
  ReadConnectionSettings;
  DefaultValueByType := TDictionary<Integer, String>.Create;
  UpdateStatementFormats := TDictionary<Integer, String>.Create;
  PopulateDefaultValuesDict(DefaultValueByType);
  PopulateUpdateStatmentsFormatDict(UpdateStatementFormats);
  // cb.ValuesAreFlags:=false;
  // LoadModule('ole32.dll',nil);
end;


{
Advantage Data Types
  1 : Logical
  2 : Numeric
  3 : DATE
  4 :
  5 :
  6 : Binary
  7 : Image
  8 : Varchar
  9 :
 10 : Double
 11 : Integer
 12 : Shortint
 13 : Time
 14 :
 15 :
 16 :
 17 :
 18 :
 19 :
 20 :
 21 :
 22 :
 23 : varcharfox
 28 : nMemo
 29 : Guid
}


procedure TfrmRepl.PopulateDefaultValuesDict(Dict : TDictionary<Integer, String>);
const
  zero = '0';
  emptyString = '''''';
  date = 'CAST(''1900-01-01'' AS SQL_DATE)';
  dateTime = 'CAST(''1900-01-01 00:00:00'' AS SQL_TIMESTAMP)';
  time = 'CAST(''00:00:00'' AS SQL_TIME)';
begin
  Dict.AddOrSetValue(1, zero);
  Dict.AddOrSetValue(2, zero);
  Dict.AddOrSetValue(3, date);
  Dict.AddOrSetValue(4, emptyString);
  Dict.AddOrSetValue(5, emptyString);
  Dict.AddOrSetValue(6, zero);
  Dict.AddOrSetValue(7, zero);
  Dict.AddOrSetValue(8, emptyString);
  Dict.AddOrSetValue(10, zero);
  Dict.AddOrSetValue(11, zero);
  Dict.AddOrSetValue(12, zero);
  Dict.AddOrSetValue(13, time);
  Dict.AddOrSetValue(14, dateTime);
  Dict.AddOrSetValue(18, zero);
  Dict.AddOrSetValue(20, emptyString);
  Dict.AddOrSetValue(23, emptyString);
  Dict.AddOrSetValue(26, emptyString);
  Dict.AddOrSetValue(28, emptyString);
  Dict.AddOrSetValue(29, emptyString);
end;

procedure TfrmRepl.PopulateUpdateStatmentsFormatDict(Dict : TDictionary<Integer,String>);
const
  number = 'SET @Script = @Script + ''[:name] ='' + IIF(__new.[:name] is null,''NULL'',''0'' + trim(CAST(__new.[:name] AS SQL_VARCHAR))) + '',''';
  str = 'SET @Script = @Script + ''[:name] ='' + IIF(__new.[:name] is null,''NULL'', '''''''' +Replace(trim(CAST(__new.[:name] AS SQL_VARCHAR)),'''''''', '''''''''''') + '''''''') + '','' ';
  other = 'SET @Script = @Script + ''[:name] ='' + IIF(__new.[:name] is null,''NULL'', trim(CAST(__new.[:name] AS SQL_VARCHAR)))  + '',''';
begin
  Dict.AddOrSetValue(1, other);
  Dict.AddOrSetValue(2, number);
  Dict.AddOrSetValue(3, str);
  Dict.AddOrSetValue(4, str);
  Dict.AddOrSetValue(5, str);
  Dict.AddOrSetValue(6, other);
  Dict.AddOrSetValue(7, other);
  Dict.AddOrSetValue(8, str);
  Dict.AddOrSetValue(10, number);
  Dict.AddOrSetValue(11, number);
  Dict.AddOrSetValue(12, number);
  Dict.AddOrSetValue(13, str);
  Dict.AddOrSetValue(14, str);
  Dict.AddOrSetValue(18, other);
  Dict.AddOrSetValue(20, str);
  Dict.AddOrSetValue(23, other);
  Dict.AddOrSetValue(26, other);
  Dict.AddOrSetValue(28, str);
  Dict.AddOrSetValue(29, other);
end;

procedure TfrmRepl.FormDestroy(Sender: TObject);
begin
  lstCols.Free;
  tmpLst.Free;
  if assigned(DefaultValueByType) then
    DefaultValueByType.Free;
  if assigned(UpdateStatementFormats) then
    UpdateStatementFormats.Free;
  if Assigned(EditedTriggers) then
    EditedTriggers.Free;
end;

function TfrmRepl.ExcludeStrs(sLst, sLstExclude: string): string;
var
  I: Integer;
begin
  Result := sLst;
  if sLstExclude = '' then
    exit;
  if Result = '' then
    exit;
  tmpLst.Text := StringReplace(sLstExclude, ',', #13#10, [rfReplaceAll]);
  Result := Result + ',';
  for I := 0 to tmpLst.Count - 1 do
    Result := StringReplace(Result, tmpLst[I] + ',', '', [rfReplaceAll]);
  if Length(Result) > 1 then
    Result := Copy(Result, 1, Length(Result) - 1);
end;

function TfrmRepl.ExecSql(Sql: string; SepCommandsDelimiter: Char = #0): variant;
var
  qry: TAdsQuery;
  flQry: Boolean;
  vDescr, vRecs, vRec: variant;
  CurRec, I: Integer;
  lstCommands: TStringList;
  sValue: string;
begin
  Result := 0;
  lstCommands := TStringList.Create;
  try
    if not con.IsConnected then
      con.IsConnected := True;

    lstCommands.LineBreak := SepCommandsDelimiter;
    lstCommands.Text := Sql; // Replace(Replace(Sql, #13#10, ' '), SepCommandsDelimiter, #13#10);

    flQry := (UpperCase(Copy(trim(Sql), 1, 6)) = 'SELECT') and (lstCommands.Count = 1);

    qry := qryExec;
    qry.Close;
    qry.Sql.Text := Sql;
    if not flQry then
      con.BeginTransaction;
    try
      if flQry then
      begin
        try
          WriteToLog('Before Open - 1 ' + qry.Sql.Text, 'trc.log');
          qry.Open;
          WriteToLog('After Open - 1', 'trc.log');
        except
          on e: Exception do
          begin
            if (Pos('NativeError = 5098', e.Message) > 0) and (Pos('Table name: ', e.Message) > 0) then
            begin
              sValue := Copy(e.Message, Pos('Table name: ', e.Message) + 12, 100);
              qry.AdsStmtSetTablePassword(sValue, DDCryPas);
              qry.Open;
            end
            else
              raise;
          end;
        end;

        vDescr := VarArrayCreate([0, qry.FieldCount - 1], varVariant);
        for I := 0 to qry.FieldCount - 1 do
          vDescr[I] := VarArrayOf([qry.Fields[I].FieldName, qry.Fields[I].DataType, qry.Fields[I].Size]);

        vRecs := VarArrayCreate([0, qry.RecordCount - 1], varVariant);
        CurRec := 0;
        qry.First;
        while not qry.EOF do
        begin
          vRec := VarArrayCreate([0, qry.FieldCount - 1], varVariant);
          for I := 0 to qry.FieldCount - 1 do
            vRec[I] := qry.Fields[I].AsString;

          vRecs[CurRec] := vRec;
          qry.Next;
          Inc(CurRec);
        end;
{$IFDEF FULLDEBUG}
        WriteToLog(Format('ExecSQL Record Count: %d', [CurRec]), 'debug.log');
{$ENDIF}
        qry.Close;
        Result := VarArrayCreate([0, 1], varVariant);
        Result[0] := vDescr;
        Result[1] := vRecs;
      end
      else
      begin
        for I := 0 to lstCommands.Count - 1 do
        begin
          // qry.Sql.Text := StringReplace(lstCommands[i], '/', '', []);
          qry.Sql.Text := lstCommands[I];
          if trim(qry.Sql.Text) <> '' then
            try
              qry.ExecSql;
            except
              on e: Exception do
              begin
                if (Pos('NativeError = 5098', e.Message) > 0) and (Pos('Table name: ', e.Message) > 0) then
                begin
                  sValue := Copy(e.Message, Pos('Table name: ', e.Message) + 12, 100);
                  qry.AdsStmtSetTablePassword(sValue, DDCryPas);
                  WriteToLog(Format('Encrypted Table: %s', [sValue]));
                  qry.ExecSql;
                end
                else
                  raise;
              end;
            end;
        end;
        con.Commit;
        Result := 1;
      end;
    except
      on e: Exception do
      begin
        Result := 'Exception : ' + e.Message + #13#10 + Sql;
        WriteToLog('Exception (1) : ' + e.Message + #13#10 + Sql, 'err.log');
        if not flQry then
          con.Rollback;
      end;
    end;
  finally
    lstCommands.Free;
  end;
end;

procedure TfrmRepl.conAfterConnect(Sender: TObject);
var
  ACon: TAdsConnection;
begin
  ACon := TAdsConnection(Sender);
  try
    ACon.Execute('Try Execute Procedure cdUpdateCrUseData(); Catch All End;');
  except
  end;
  try
    // ExecSql('drop table sync_settings');
    try
      qryExec.Sql.Text := 'select ins_fields from sync_settings where 1=0';
      qryExec.Open;
      qryExec.Close;
    except
      on e: Exception do
      begin
        try
          ExecSql('drop table sync_settings');
        except
        end;
        ExecSql(ReadTemplateRes('sync_settings_sql', []));
      end;
    end;
  except
  end;
  btnRefreshTabsClick(nil);
end;

procedure TfrmRepl.conBeforeConnect(Sender: TObject);
begin
  TAdsConnection(Sender).Password := UserPass;
end;

procedure TfrmRepl.CreateTriggers;
begin
  if qryTabsins.AsInteger = 1 then
    create_trig('ins');
  if qryTabsupd.AsInteger = 1 then
    create_trig('upd');
  if qryTabsdel.AsInteger = 1 then
    create_trig('del');
end;


/// <summary>
/// Creates a trigger based on the specified type (ins,upd,del).
/// </summary>
/// <param name="sType">The type of trigger to create.</param>
procedure TfrmRepl.create_trig(sType: string = '');
var
  Sql: string;
  tabName: string;
  sCols, iVals, uSelFields, uWhereKey, keys, sFields, sColsRestr, upd_resource_name: string;
  // lstSplit: TStringList;
begin
  if sType = '' then
    exit;

  tabName := qryTabsTable.AsString;
  if qryTabskeys.AsString = '' then
    exit;

  keys := '''' + ReplaceAll(qryTabskeys.AsString, [' ', '', ',', ''',''']) + '''';
  // uWhereKey are the where keys that are used to identify included records
  uWhereKey := GetSQLValue(ReadTemplateRes('upd_where_keys_sql', [':tab', tabName, ':keys', keys]));

  sFields := trim(qryTabs.FieldByName(sType + '_fields').AsString);
  if sFields = '' then
    sFields := get_cols
  else
    sFields := StringReplace(sFields, ';', ',', [rfReplaceAll]);
  // qryTabsFields.AsString;//StringReplace(lstSplit[1], #13#10, '', [rfReplaceAll]);

  sColsRestr := '''' + ReplaceAll(sFields, [' ', '', ',', ''',''']) + '''';

  if qryTabs.FieldByName(sType + '_Exc').AsInteger = 1 then
  begin
    sFields := get_cols(sColsRestr);
    sColsRestr := '''' + ReplaceAll(sFields, [' ', '', ',', ''',''']) + '''';
  end;

  {in order to create the triggers we seem to read the template from resource files}
  case sType[1] of
    'i':
      begin
        sCols := GetSQLValue(ReadTemplateRes('sel_cols_sql', [':tab', tabName, ':cols_restrict', sColsRestr]), '', ',');
        iVals := GetSQLValue(ReadTemplateRes('ins_vals_sql', [':tab', tabName, ':cols_restrict', sColsRestr]));
        Sql := ReadTemplateRes('ins_trg', [':tab', tabName, ':cols', sCols, ':sel_vals', iVals, ':WhereKey',
          uWhereKey]);
      end;
    'u':
      begin
        {
          Algo:
            1. Read template from the resource file upd_sel_flds_sql
            2. replace the placeholders in the template file with the param key/values
            3. execute the query in the template file to get all the field updates
            4. add the generated fields template to the upd_trg template file
        }
        if tglswtchNewOld.State = tssOff then
        begin
          uSelFields := GenerateUpdateStatements(GetIncludedColumns(tabName, sColsRestr, qryExec));
          if uSelFields.IsEmpty then
            Exit;
          uWhereKey := StringReplace(uWhereKey, '([', '(__old.[', [rfReplaceAll]);
          upd_resource_name := 'upd_trg';
        end
        else
        begin
          uSelFields := GetSQLValue(ReadTemplateRes('upd_sel_flds_sql', [':tab', tabName, ':cols_restrict', sColsRestr]));
          upd_resource_name := 'upd_prev';
        end;

        Sql := ReadTemplateRes(upd_resource_name, [':tab', tabName, ':WhereKey', uWhereKey, ':SelFields', uSelFields]);
      end;
    'd':
      Sql := ReadTemplateRes('del_trg', [':tab', tabName, ':WhereKey', uWhereKey]);
  end;
  try
    ExecSql(Sql);
  except
    // Ignore It...
  end;
end;

{
   1. Function GetIncludedColumns(TableName, ColumnsToInclude : String; Query:TAdsQuery) : TDataSet;
   2. Function GenerateUpdateStatements(ColumnData : TDataSet) : String;
   3. Function GetStatement(ColumnName : String; FieldType : Integer): String;
}

Function TfrmRepl.GetIncludedColumns(TableName, ColumnsToInclude : String; Query:TAdsQuery) : TDataSet;
const
  SqlStatment = 'SELECT NAME, FIELD_TYPE FROM SYSTEM.COLUMNS WHERE PARENT = %s and name in (%s);';
begin
  Query.close;
  Query.sql.text := Format(SqlStatment, [TableName.QuotedString, ColumnsToInclude]);
  try
    Query.open;
    Result := Query;
  except
    on E: Exception do
    begin
      Result := nil;
    end;
  end;
end;

Function TfrmRepl.GenerateUpdateStatements(ColumnData : TDataSet) : String;
const
  NAME = 'NAME';
  FIELD_TYPE = 'FIELD_TYPE';
var
  ColumnName : String;
  FieldType : Integer;
begin
  Result := '';
  if ColumnData = nil then
    exit;

  ColumnData.First;
  while not ColumnData.Eof do
  begin
    ColumnName := ColumnData.FieldByName(NAME).AsString;
    FieldType := ColumnData.FieldByName(FIELD_TYPE).AsInteger;
    Result := Result + GetStatement(ColumnName, FieldType);
    ColumnData.Next;
  end;
end;

Function TfrmRepl.GetStatement(ColumnName : String; FieldType : Integer): String;
begin
  Result := '';
  try
    if not (DefaultValueByType.ContainsKey(FieldType) AND UpdateStatementFormats.ContainsKey(FieldType)) then
      raise Exception.Create(format('Column %s has an Unsupported Data Type : %d', [ColumnName, FieldType]));


    Result := Format('IF (IFNULL(__new.[:name], %s) <> IFNULL(__old.[:name], %s)) Then %s', [DefaultValueByType.Items[FieldType], DefaultValueByType.Items[FieldType], sLineBreak]);
    Result := Result + UpdateStatementFormats.Items[FieldType] + Format('; --FieldType=%d', [FieldType]) + sLineBreak;
    Result := Result + 'END IF;' + sLineBreak;
    Result := StringReplace(Result,':name', ColumnName, [rfReplaceAll]);
  finally
  end;
end;

procedure TfrmRepl.DropTriggers;
var
  tabName: string;
begin
  try
    tabName := qryTabsTable.AsString;
    ExecSql(GetSQLValue
      ('select ''drop trigger ''+trig_tablename+''.''+name+''/'' from system.triggers where trig_tablename=''' + tabName
      + ''' and name like ''t__sync__%'''), '/');
  except
  end;
  // ExecSql(Format('drop trigger %s.t%s_sync_%s',[tabName,trgType,tabName]))
end;

procedure TfrmRepl.dsTabsDataChange(Sender: TObject; Field: TField);
begin
  Upd_Trig_Source_Panel;
end;

procedure TfrmRepl.dsTabsUpdateData(Sender: TObject);
begin
  //
end;

procedure TfrmRepl.edFltTabChange(Sender: TObject);
begin
  if edFltTab.Text = '' then
    qryTabs.Sql.Text := mainSql
  else
    qryTabs.Sql.Text := Replace(mainSql, 'order by', ' where "Table" like ''' + edFltTab.Text + '%'' order by ');
  upd_sync_settings;
end;

procedure TfrmRepl.edFltTabEnter(Sender: TObject);
begin
  cb.Visible := false;
end;

procedure TfrmRepl.edFltTabKeyPress(Sender: TObject; var Key: Char);
begin
  //
end;

procedure TfrmRepl.btnCreateTriggersClick(Sender: TObject);
var
  tabName: string;
begin
  // ReadConnectionSettings;

  ExecSql(GetSQLValue
    ('select ''drop trigger ''+trig_tablename+''.''+name+''/'' from system.triggers where name like ''t__sync__%'''),
    '/');

  ExecSql('CREATE TABLE Sync (ID AutoInc CONSTRAINT PK_SYNC PRIMARY KEY CONSTRAINT NOT NULL,EXPR Char(15000),TableName CIChar(50),CallType CIChar(15),DateTimeStmp ModTime) IN DATABASE');
  ExecSql('Alter Table Sync Add Column TableName CIChar( 50 ) Add Column CallType CIChar( 15 ) Add Column DateTimeStmp ModTime;');
  ExecSql('EXECUTE PROCEDURE sp_ModifyTableProperty( ''Sync'', ''Table_Auto_Create'', ''True'', ''APPEND_FAIL'', ''SyncFail'')');
  ExecSql('CREATE TABLE sync_msg(ID integer CONSTRAINT NOT NULL,comp Char(250) CONSTRAINT NOT NULL) IN DATABASE');
  ExecSql('CREATE INDEX IDX_SYNC_MSG_ID ON SYNC_MSG(ID)');
  ExecSql('CREATE INDEX IDX_SYNC_MSG_COMP ON SYNC_MSG(COMP)');
  ExecSql('EXECUTE PROCEDURE sp_ModifyTableProperty( ''Sync_Msg'', ''Table_Auto_Create'', ''True'', ''APPEND_FAIL'', ''SyncFail'')');
  ExecSql('CREATE TABLE Sync_Comp(comp Char(250) CONSTRAINT PK_SYNC_COMP PRIMARY KEY CONSTRAINT NOT NULL,lastsync TimeStamp default ''Now()'') IN DATABASE');
  ExecSql('EXECUTE PROCEDURE sp_ModifyTableProperty( ''Sync_Comp'', ''Table_Auto_Create'', ''True'', ''APPEND_FAIL'', ''SyncFail'')');
  ExecSql(ReadTemplateRes('ins_sync_trg', []));
  ExecSql(ReadTemplateRes('del_sync_comp_trg', []));
  ExecSql(ReadTemplateRes('del_sync_msg_trg', []));
  ExecSql(ReadTemplateRes('Sync_GetUpdExpr_sql', []));

  tabName := qryTabsTable.AsString;
  qryTabs.DisableControls;
  try
    qryTabs.First;
    while not qryTabs.EOF do
    begin
      try
        CreateTriggers;
      except
      end;
      qryTabs.Next;
    end;
    qryTabs.Locate('Table', tabName, [loCaseInsensitive]);
  finally
    qryTabs.EnableControls;
  end;
  // for i := 0 to mTabs.Lines.Count - 1 do
  // CreateTriggers(mTabs.Lines.Names[i], mTabs.Lines.ValueFromIndex[i]);

  if Visible then
    ShowMessage('Triggers Successfully Created.');
end;

function TfrmRepl.GetSQLValue(SQLText: string; Delim4Fields: string = ';'; Delim4Recs: string = #13#10): string;
var
  V, vi: variant;
  // val: string;
  I, j, cntRecs, cntFields: Integer;
begin
  Result := '';
  // Caption:=SQLText;
  // Application.ProcessMessages;
{$IFDEF FULLDEBUG}
  WriteToLog(Format('GetSQLValue : %s', [SQLText]), 'debug.log');
{$ENDIF}
  V := ExecSql(SQLText);
  cntRecs := VarArrayHighBound(V[1], 1);
  if cntRecs < 0 then
    exit;
  cntFields := VarArrayHighBound(V[1][0], 1);
  V := V[1];
  for I := 0 to cntRecs do
  begin
    // Continue;
    vi := V[I];
    for j := 0 to cntFields do
    begin
      // val := VarToStr(vi[j]);
      // val := V[1][i][j];
      // val:=vi[j];
      Result := Result + vi[j]; // val;
      if j <> cntFields then
        Result := Result + Delim4Fields;
    end;
    if I <> cntRecs then
      Result := Result + Delim4Recs;
  end;
end;

function TfrmRepl.get_cols(sExclCols: string = ''): string;
begin
  Result := lstCols.Values[qryTabsTable.AsString];
  Result := ExcludeStrs(Result, sExclCols);
end;

procedure TfrmRepl.grTabsCellClick(Column: TColumn);
begin
  if (Column.Field.DataType = ftInteger) then
  begin
    ShowTab(Column);
    upd_sync_settings(Column.FieldName, IntToStr(Abs(Column.Field.AsInteger - 1)));
  end;
end;

function TfrmRepl.OverwriteCurrentTrigger(msg : String = '') : Boolean;
const
  WarningMsg = 'This action will delete/overwrite any previously stored trigger with the same name. Are you sure you would like to continue? This action cannot be reversed.';
var
  Warning : String;
begin
  Warning := msg;
  if Warning.IsEmpty then
     Warning := WarningMsg;

  Result := MessageDlg(Warning, mtWarning, [mbYes, mbNo], 0) = mrYes;
end;

// Show the correct tab based on which column was selected
procedure TfrmRepl.ShowTab(Column: TColumn);
const
  TriggerType : array of string = ['ins', 'upd', 'del'];
var
  I : Integer;
begin
  for I := 0 to 2 do
  begin
   if SameText(Column.Field.DisplayName, TriggerType[I]) then
   begin
    trgRG.ItemIndex := I;
    Break;
   end;
  end;
end;

procedure TfrmRepl.grTabsColExit(Sender: TObject);
begin
  cb.Visible := false;
end;

procedure TfrmRepl.grTabsDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
const
  CtrlState: array [Boolean] of Integer = (DFCS_BUTTONCHECK, DFCS_BUTTONCHECK or DFCS_CHECKED);
begin
  if (Column.Field.DataType = ftInteger) then
  begin
    grTabs.Canvas.FillRect(Rect);
    DrawFrameControl(grTabs.Canvas.Handle, Rect, DFC_BUTTON, CtrlState[Column.Field.AsInteger = 1]);
    { checked or unchecked }
  end
  else
    grTabs.DefaultDrawColumnCell(Rect, DataCol, Column, State);
  if (gdFocused in State) and (Column.ButtonStyle = cbsEllipsis) then
  // (pos('keys',LowerCase(column.Field.FieldName))>=1) then
  begin
    cb.Left := Rect.Left + grTabs.Left + 2;
    cb.Top := Rect.Top + grTabs.Top + Panel2.Top + 2;
    cb.Width := Rect.Right - Rect.Left;
    cb.Height := Rect.Bottom - Rect.Top;
    cb.Clear;
    cb.Text := Column.Field.AsString;
    lastModField := Column.Field.FieldName;
    cb.Visible := True;
  end;
end;

procedure TfrmRepl.init;
const
  sqlCols = 'select parent, name from system.columns where Field_Type<>22 And Field_Type<>15 And Field_Type<>21 order by parent, name';
  // upper(parent)=''%s'' And
var
  I: Integer;
  sCols, tName: string;
  lst: TStringDynArray;
begin
  tName := '';
  sCols := '';
  tmpLst.Text := GetSQLValue(sqlCols);
  for I := 0 to tmpLst.Count - 1 do
  begin
    lst := SplitString(tmpLst[I], ';');
    if High(lst) <> 1 then
      raise Exception.Create('error : Columns info. ');

    if tName <> lst[0] then
    begin
      lstCols.Values[tName] := Copy(sCols, 2, Length(sCols));
      tName := lst[0];
      sCols := '';
    end;
    sCols := sCols + ',' + lst[1];
  end;
end;

function TfrmRepl.SetNewConnection(AInitialDir: string): string;
begin
  Result := '';
  if AInitialDir <> '' then
    oDlg.InitialDir := AInitialDir
  else
    oDlg.InitialDir := ExtractFileDir(Application.ExeName);
{$IFDEF SYRCLIENT}
  if MessageBox(Handle, PChar('Browse for folder with free tables?'), 'DB', MB_YESNO) = ID_YES then
  begin
    if not SelectDirectory('Select folders with free tables', '', Result) then
    begin
      exit;
    end;
  end
  else
{$ENDIF}
  begin
    if not oDlg.Execute then
    begin
      exit;
    end;
    Result := oDlg.FileName;
  end;
  edCon.Text := Result;
end;

procedure TfrmRepl.trgRGClick(Sender: TObject);
begin
  Upd_Trig_Source_Panel;
end;

procedure TfrmRepl.upd_sync_settings(fldName: string = ''; Val: string = '');
var
  sSql, tabName: string;
  GridRow, TotalRow: Integer;
begin
  tabName := qryTabsTable.AsString;
  if fldName <> '' then
  begin

    if not OverwriteCurrentTrigger then
      Exit;

    if not EditedTriggers.ContainsKey(tabName) then
    begin
      ShowMessage('A backup of sync triggers for ' + tabName + ' have been saved under the directory "TriggerHistory\' + tabName + '"');
      EditedTriggers.add(tabName,  True);
      SaveTriggerHistoryForTable(tabName);
    end;

    DropTriggers;
    if StrToIntDef(Val, -1) = -1 then
      Val := '''' + Val + '''';
    if qryTabsExist.AsInteger = 0 then
      sSql := Format('INSERT into sync_settings(table_name,%s) VALUES (''%s'', %s)', [fldName, tabName, Val])
    else
      sSql := Format('update sync_settings set %s=%s where table_name=''%s''', [fldName, Val, tabName]);
    ExecSql(sSql);
  end;
  cb.Visible := false;
  // GetScrollInfo(grTabs.Handle, SB_VERT, sInf);
  GridRow := TGridCracker(grTabs).row;
  TotalRow := TGridCracker(grTabs).RowCount;
  // dy := TGridCracker(grTabs).VisibleRowCount;
  // row:=TGridCracker(grTabs).TopRow;
  qryTabs.DisableControls;
  try
    qryTabs.Close;
    qryTabs.Open;
    qryTabs.Locate('Table', tabName, [loCaseInsensitive]);
    if fldName <> '' then
    begin
      CreateTriggers;
      qryTabs.Close;
      qryTabs.Open;
      qryTabs.Locate('Table', tabName, [loCaseInsensitive]);
    end;
  finally
    Dec(TotalRow);
    if GridRow < TotalRow div 2 then
    begin
      grTabs.DataSource.DataSet.MoveBy(TotalRow - GridRow);
      grTabs.DataSource.DataSet.MoveBy(GridRow - TotalRow);
    end
    else
    begin
      if dgTitles in grTabs.Options then
        Dec(GridRow);
      grTabs.DataSource.DataSet.MoveBy(-GridRow);
      grTabs.DataSource.DataSet.MoveBy(GridRow);
    end;
    // grTabs.DataSource.DataSet.MoveBy(-row);
    qryTabs.EnableControls;
  end;
  // SetScrollInfo(grTabs.Handle, SB_VERT, sInf,True);
end;

procedure TfrmRepl.btnRefreshTabsClick(Sender: TObject);
begin
  init;
  upd_sync_settings;
end;

procedure TfrmRepl.btnSaveColumnsClick(Sender: TObject);
begin
  if qryTabs.FieldByName('keys').AsString.IsEmpty then
  begin
    Showmessage('Keys cannot be empty. In order to save the columns you first need to save the keys.');
    exit;
  end;


  //extract keys and insert into settings table
  if OverwriteCurrentTrigger('This Action will overwrite the insert and update trigger for this table. Are you sure you want to continue?') then
  begin
    upd_sync_settings('ins_fields', mmoColumns.Text);
    upd_sync_settings('upd_fields', mmoColumns.Text);
  end;
end;

procedure TfrmRepl.btnSaveKeysClick(Sender: TObject);
begin
  //extract keys and insert into settings table
  if OverwriteCurrentTrigger('This Action will delete all the stored sync triggers for this table. If you want to save the keys/columns for any of the trigger types copy the text from the memo box before preceding. Are you sure you want to continue?') then
  begin
      //force a save of the current triggers to a text file before updating the keys
      upd_sync_settings('keys', mmoKeys.Text);
  end;
end;

procedure TfrmRepl.SaveTriggerHistoryForTable(TableName : String);

function FileName(Name : String) : String;
begin
  Result := TableName + '_' + Name;
end;

var
  Insert, Update, Delete : String;
begin
  Insert := qryTabs.FieldByName('i_s').AsString;
  Update := qryTabs.FieldByName('u_s').AsString;
  Delete := qryTabs.FieldByName('d_s').AsString;
  SaveTriggerHistory([FileName('insert'), FileName('update'), FileName('delete')], [Insert, Update, Delete] , 'TriggerHistory', TableName);
end;

procedure TfrmRepl.btnSaveTriggerToFileClick(Sender: TObject);
begin
  if dlgSave.Execute then
    Save2File(dlgSave.FileName, GetCustomTriggerTextOutput);
end;

function TfrmRepl.GetCustomTriggerTextOutput : AnsiString;
begin
  Result := '--KEYS' + sLineBreak;
  Result := Result + '/*' + CurrentTrigger.Keys + '*/' + sLineBreak;
  Result := Result + sLineBreak + '--COLUMNS' + sLineBreak;
  Result := Result +'/*' + CurrentTrigger.Columns + '*/' + sLineBreak;;
  Result := Result + sLineBreak + '--TRIGGER' + sLineBreak;
  Result := Result + CurrentTrigger.TriggerCode;
end;

procedure TfrmRepl.btnSetConClick(Sender: TObject);
begin
  SetNewConnection(edCon.Text);
  CreateRegKey(CustomReg, 'Server', edCon.Text, HKEY_LOCAL_MACHINE);
  ReadConnectionSettings;
end;

procedure TfrmRepl.btnTrigSaveClick(Sender: TObject);
begin
  if dlgSave.Execute then
    Save2File(dlgSave.FileName, trgSource.Text);
end;

procedure TfrmRepl.cmbCloseUp(Sender: TObject);
begin
  if lastModField <> '' then
    upd_sync_settings(lastModField, cb.Text);
  lastModField := '';
end;

procedure TfrmRepl.cmbDropDown(Sender: TObject);
begin
  cb.Items.Text := Replace(get_cols, ',', #13#10);
  // lastModField:=column.Field.FieldName;
  cb.Caption := qryTabs.FieldByName(lastModField).AsString;
end;

procedure TfrmRepl.ReadConnectionSettings;
var
  ConStr: string;
begin
  con.IsConnected := false;
  ConStr := GetRegStringValue(CustomReg, 'Server', HKEY_LOCAL_MACHINE);
  if ConStr = '' then
  begin
    if IsDelphiInstalled then
      ConStr := '\\192.168.0.112:6262\Data\Pos\TisWin3DD.add'
    else
      ConStr := 'F:\POS\TisWin3DD.add';
  end;
  edCon.Text := ConStr;
  con.ConnectPath := ConStr;
  con.Username := 'Dev';
  //if FileExists(con.ConnectPath) then
  if con.ConnectPath <> '' then
    con.IsConnected := True;
end;

procedure TfrmRepl.tglswtchNewOldClick(Sender: TObject);
const
  WarningMsg = 'The previous version of the sync trigger is significantly slower and is only included to maintain backward compatibility. Are you sure you want to continue?';
var
  Result : Boolean;
begin
  if tglswtchNewOld.State = tssON then
  begin
    Result := MessageDlg(WarningMsg, mtWarning, [mbYes, mbNo], 0) = mrYes;
    if not Result then
      tglswtchNewOld.State := tssOff;
  end;
end;

//NOTE : This procedure sets the memo box with the trigger ouput
procedure TfrmRepl.Upd_Trig_Source_Panel;
var
  sText, tabName: string;
begin
  tabName := qryTabsTable.AsString;
  // the trigger is read from a result of the query executed by qryTabs
  // trgRG.ItemIndex is the index of the radio group
  // trig_abbr is an array of strings ('i', 'u', 'd') -- used as a prefix for the trigger name
  sText := trim(qryTabs.FieldByName(trig_abbr[trgRG.ItemIndex] + '_s').AsString);
  if sText <> '' then
  begin
    sText:=  Format('create trigger t%s_sync_%s on %s after %s'#13#10'begin' + #13#10, [trig_abbr[trgRG.ItemIndex], tabName, tabName, trig_oper[trgRG.ItemIndex]]) + sText + #13#10 + 'end;';
    trgSource.Text := sText;
    ShowCustomTriggerPanel(sText);
  end
  else
    trgSource.Text := '';
end;

procedure TfrmRepl.ShowCustomTriggerPanel(TriggerText : String);
begin
   if qryTabs.FieldByName('keys').AsString.IsEmpty then
    begin
      dsTabs.OnDataChange := nil;
      try
        var ExtractedFields : TArray<String> := ExtractFieldsFromTrigger(TriggerText);
        if ExtractedFields[0].IsEmpty then
          raise Exception.Create('Cant read where keys from custom trigger.');

        if ExtractedFields[1].IsEmpty and SameText(tab_names[trgRG.ItemIndex], 'upd') then
          raise Exception.Create('Can''t read column name from custom trigger.');

        CurrentTrigger.TriggerType := trig_oper[trgRG.ItemIndex];
        CurrentTrigger.Keys := ExtractedFields[0];
        CurrentTrigger.Columns := ExtractedFields[1];
        CurrentTrigger.TriggerCode := TriggerText;
        CurrentTrigger.TriggerFieldPrefix := format('%s_fields' ,[tab_names[trgRG.ItemIndex]]);
        SetCustomTriggerPanel(CurrentTrigger);
      finally
         dsTabs.OnDataChange := dsTabsDataChange;
      end;
    end;
end;

procedure TfrmRepl.SetCustomTriggerPanel(Trig : Trigger);
begin
  Panel5.Visible := True;
  Panel5.BringToFront;
  lblTriggerType.Caption := 'Trigger Type : '+ Trig.TriggerType;
  mmoCustomTrigger.Text := Trig.TriggerCode;
  mmoKeys.Text := Trig.Keys;
  mmoColumns.Text := Trig.Columns;
end;

function TfrmRepl.ExtractFieldsFromTrigger(TriggerCode : String) : TArray<string>;

function hasMatch(substr, txt : String) : Boolean;
begin
  Result := Pos(substr, txt) > 0;
end;

function GetStringList : TStringList;
begin
  Result := TStringList.Create;
  Result.Duplicates := dupIgnore;
  Result.Delimiter :=',';
  Result.Sorted := True;
end;

procedure AddColumnToStringList(list : TStringList; Line : String);
const
  ColumnNameRegex = '\[(\w+)\]';
var
  Match: TMatch;
  ColumnName: string;
begin
    Match := TRegEx.Match(Line, ColumnNameRegex);
    if Match.Success then
    begin
      ColumnName := Match.Groups[1].Value;
      list.Add(ColumnName);
    end;
end;

var
  UniqueColumns, WhereColumns: TStringList;
  Line: string;
  Match: TMatch;
  ColumnName: string;
  IsKey, IsColumn : Boolean;
begin
  SetLength(Result, 2);
  UniqueColumns := GetStringList;
  WhereColumns := GetStringList;
  try
    // Find unique columns
    for Line in TriggerCode.Split([sLineBreak]) do
    begin
      IsKey := hasMatch('CONVERT(__old.[', line) or hasMatch('MERGE Prdmstr ON', line) or hasMatch('],SQL_VARCHAR)+', line);
      IsColumn := hasMatch('IF (IFNULL(__new.[', Line) or hasMatch('IIF', Line) or hasMatch('trim(Sync_GetUpdExpr', line);

      if IsColumn then
        AddColumnToStringList(UniqueColumns, line)
      else if IsKey then
        AddColumnToStringList(WhereColumns, line);
    end;
    Result[0] := WhereColumns.DelimitedText;
    Result[1] := UniqueColumns.DelimitedText;
  finally
    UniqueColumns.Free;
    WhereColumns.Free;
  end;
end;

function TfrmRepl.UserPass: string;
var
  I: Integer;
begin
  Result := 'l' + chr(101) + chr(122) + 'a' + chr(77);
  for I := 1 to 5 do
    Result := Result + IntToStr(I);
  Result := Result + '$$';
end;

initialization

finalization

end.
