object frmRepl: TfrmRepl
  Left = 340
  Top = 251
  Caption = 'CompuDime Sync Setup'
  ClientHeight = 731
  ClientWidth = 1224
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poMainFormCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  TextHeight = 13
  object Panel5: TPanel
    Left = 575
    Top = 76
    Width = 649
    Height = 614
    Align = alRight
    Caption = 'Panel5'
    ShowCaption = False
    TabOrder = 4
    ExplicitLeft = 571
    ExplicitHeight = 613
    object mmoCustomTrigger: TMemo
      Left = 344
      Top = 1
      Width = 304
      Height = 571
      Align = alRight
      Lines.Strings = (
        'mmoCustomTrigger')
      TabOrder = 0
      ExplicitHeight = 570
    end
    object pnl2: TPanel
      Left = 1
      Top = 1
      Width = 343
      Height = 571
      Align = alClient
      TabOrder = 1
      ExplicitHeight = 570
      object lblTriggerType: TLabel
        Left = 1
        Top = 1
        Width = 341
        Height = 25
        Align = alTop
        Alignment = taCenter
        Caption = 'Trigger Type'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -21
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ExplicitWidth = 112
      end
      object lblWhereKeys: TLabel
        Left = 72
        Top = 117
        Width = 23
        Height = 13
        Caption = 'Keys'
      end
      object lblColumns: TLabel
        Left = 232
        Top = 117
        Width = 40
        Height = 13
        Caption = 'Columns'
      end
      object mmoKeys: TMemo
        Left = 16
        Top = 136
        Width = 145
        Height = 121
        Lines.Strings = (
          'mmoKeys')
        TabOrder = 0
      end
      object mmoColumns: TMemo
        Left = 184
        Top = 136
        Width = 145
        Height = 121
        Lines.Strings = (
          'mmoColumns')
        TabOrder = 1
      end
      object btnSaveTriggerToFile: TButton
        AlignWithMargins = True
        Left = 21
        Top = 525
        Width = 301
        Height = 25
        Margins.Left = 20
        Margins.Right = 20
        Margins.Bottom = 20
        Align = alBottom
        Caption = 'Save Trigger to Text File'
        TabOrder = 2
        OnClick = btnSaveTriggerToFileClick
        ExplicitLeft = 144
        ExplicitTop = 488
        ExplicitWidth = 150
      end
    end
    object pnlBottomBar: TPanel
      Left = 1
      Top = 572
      Width = 647
      Height = 41
      Align = alBottom
      Caption = 'pnlBottomBar'
      ShowCaption = False
      TabOrder = 2
      ExplicitTop = 571
      object btnClosePanel: TButton
        AlignWithMargins = True
        Left = 568
        Top = 4
        Width = 75
        Height = 33
        Align = alRight
        Caption = 'Close'
        TabOrder = 0
        OnClick = btnClosePanelClick
      end
    end
    object btnSaveKeys: TButton
      Left = 56
      Top = 275
      Width = 75
      Height = 25
      Hint = 
        'Clicking on Set Keys will set the keys from this memo box to the' +
        ' selected table keys.'#13#10
      Caption = 'Set Keys'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      OnClick = btnSaveKeysClick
    end
    object btnSaveColumns: TButton
      Left = 224
      Top = 275
      Width = 75
      Height = 25
      Hint = 
        'Clicking on Set Columns will set the columns from this memo box ' +
        'to the selected table columns.'
      Caption = 'Set Columns'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 4
      OnClick = btnSaveColumnsClick
    end
  end
  object pnlBottom: TPanel
    Left = 0
    Top = 690
    Width = 1224
    Height = 41
    Align = alBottom
    TabOrder = 1
    ExplicitTop = 689
    ExplicitWidth = 1220
    object mLog: TMemo
      Left = 1
      Top = 1
      Width = 1222
      Height = 39
      Align = alClient
      TabOrder = 0
      Visible = False
      ExplicitWidth = 1218
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 76
    Width = 657
    Height = 614
    Align = alLeft
    TabOrder = 0
    ExplicitHeight = 613
    object Splitter1: TSplitter
      Left = 653
      Top = 1
      Height = 612
      Align = alRight
      Visible = False
      ExplicitLeft = 9
      ExplicitTop = 2
      ExplicitHeight = 439
    end
    object gbtabs: TGroupBox
      Left = 1
      Top = 1
      Width = 652
      Height = 612
      Align = alClient
      Caption = 'Tables'
      TabOrder = 0
      ExplicitHeight = 611
      object grTabs: TDBGrid
        Left = 2
        Top = 52
        Width = 648
        Height = 558
        Align = alClient
        DataSource = dsTabs
        DefaultDrawing = False
        Options = [dgTitles, dgIndicator, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
        ReadOnly = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnCellClick = grTabsCellClick
        OnColExit = grTabsColExit
        OnDrawColumnCell = grTabsDrawColumnCell
        Columns = <
          item
            ButtonStyle = cbsNone
            Expanded = False
            FieldName = 'Table'
            Width = 61
            Visible = True
          end
          item
            ButtonStyle = cbsEllipsis
            Expanded = False
            FieldName = 'keys'
            Width = 106
            Visible = True
          end
          item
            ButtonStyle = cbsNone
            Expanded = False
            FieldName = 'ins'
            Title.Caption = 'Insert'
            Width = 34
            Visible = True
          end
          item
            ButtonStyle = cbsNone
            Expanded = False
            FieldName = 'ins_exc'
            Title.Caption = 'Exclude'
            Width = 44
            Visible = True
          end
          item
            ButtonStyle = cbsEllipsis
            Expanded = False
            FieldName = 'ins_fields'
            Title.Caption = 'Fields'
            Width = 120
            Visible = True
          end
          item
            ButtonStyle = cbsNone
            Expanded = False
            FieldName = 'upd'
            Title.Caption = 'Update'
            Width = 39
            Visible = True
          end
          item
            ButtonStyle = cbsNone
            Expanded = False
            FieldName = 'upd_exc'
            Title.Caption = 'Exclude'
            Width = 42
            Visible = True
          end
          item
            ButtonStyle = cbsEllipsis
            Expanded = False
            FieldName = 'upd_fields'
            Title.Caption = 'Fields'
            Width = 120
            Visible = True
          end
          item
            ButtonStyle = cbsNone
            Expanded = False
            FieldName = 'del'
            Title.Caption = 'Delete'
            Width = 34
            Visible = True
          end
          item
            ButtonStyle = cbsNone
            Expanded = False
            FieldName = 'del_exc'
            Title.Caption = 'Exclude'
            Visible = False
          end
          item
            ButtonStyle = cbsEllipsis
            Expanded = False
            FieldName = 'del_fields'
            Title.Caption = 'Fields'
            Visible = False
          end>
      end
      object Panel6: TPanel
        Left = 2
        Top = 15
        Width = 648
        Height = 37
        Align = alTop
        TabOrder = 1
        object Label1: TLabel
          Left = 10
          Top = 4
          Width = 22
          Height = 13
          Caption = 'Filter'
        end
        object edFltTab: TEdit
          Left = 48
          Top = 1
          Width = 273
          Height = 19
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 0
          OnChange = edFltTabChange
          OnEnter = edFltTabEnter
          OnKeyPress = edFltTabKeyPress
        end
        object btnRefreshTabs: TBitBtn
          Left = 481
          Top = 0
          Width = 75
          Height = 25
          Caption = '&Refresh'
          Kind = bkRetry
          NumGlyphs = 2
          TabOrder = 1
          OnClick = btnRefreshTabsClick
        end
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 1224
    Height = 76
    Align = alTop
    TabOrder = 2
    ExplicitWidth = 1220
    object grpDbConnectionSet: TGroupBox
      Left = 1
      Top = 1
      Width = 982
      Height = 74
      Align = alClient
      Caption = 'Database Connection Settings'
      TabOrder = 0
      ExplicitWidth = 978
      object edCon: TEdit
        Left = 14
        Top = 20
        Width = 411
        Height = 21
        ReadOnly = True
        TabOrder = 1
      end
      object btnSetCon: TButton
        Left = 14
        Top = 47
        Width = 84
        Height = 23
        Caption = 'Set Connection'
        TabOrder = 0
        OnClick = btnSetConClick
      end
      object btnCreateTriggers: TButton
        Left = 295
        Top = 47
        Width = 130
        Height = 23
        Caption = 'Rebuild Sync Triggers'
        TabOrder = 2
        OnClick = btnCreateTriggersClick
      end
    end
    object tglswtchNewOld: TToggleSwitch
      AlignWithMargins = True
      Left = 986
      Top = 4
      Width = 231
      Height = 68
      Margins.Right = 6
      Align = alRight
      StateCaptions.CaptionOn = 'Use Previous Update Trigger Version'
      StateCaptions.CaptionOff = 'Use Previous Update Trigger Version'
      TabOrder = 1
      OnClick = tglswtchNewOldClick
      ExplicitLeft = 982
      ExplicitHeight = 20
    end
  end
  object Panel3: TPanel
    Left = 657
    Top = 76
    Width = 567
    Height = 614
    Align = alClient
    TabOrder = 3
    ExplicitHeight = 613
    object GroupBox3: TGroupBox
      Left = 1
      Top = 1
      Width = 565
      Height = 612
      Align = alClient
      Caption = 'Trigger'
      TabOrder = 0
      ExplicitHeight = 611
      object trgRG: TRadioGroup
        Left = 2
        Top = 15
        Width = 561
        Height = 37
        Align = alTop
        Columns = 3
        ItemIndex = 0
        Items.Strings = (
          'Insert'
          'Update'
          'Delete')
        TabOrder = 0
        OnClick = trgRGClick
      end
      object trgSource: TMemo
        Left = 2
        Top = 52
        Width = 561
        Height = 517
        Align = alClient
        TabOrder = 1
        ExplicitHeight = 516
      end
      object Panel4: TPanel
        Left = 2
        Top = 569
        Width = 561
        Height = 41
        Align = alBottom
        TabOrder = 2
        ExplicitTop = 568
        object btnTrigSave: TBitBtn
          Left = 96
          Top = 8
          Width = 75
          Height = 25
          Caption = 'Save'
          TabOrder = 0
          OnClick = btnTrigSaveClick
        end
      end
    end
  end
  object qryExec: TAdsQuery
    DatabaseName = 'con'
    AdsConnection = con
    Left = 148
    Top = 188
    ParamData = <>
  end
  object con: TAdsConnection
    ConnectPath = 'C:\!pro\ads\DataDemo\TisWin3DD.add'
    AdsServerTypes = [stADS_REMOTE, stADS_LOCAL]
    LoginPrompt = False
    Username = 'Dev'
    AfterConnect = conAfterConnect
    BeforeConnect = conBeforeConnect
    Left = 84
    Top = 188
  end
  object oDlg: TOpenDialog
    DefaultExt = '*.add'
    Filter = '*.add|*.add'
    Options = [ofHideReadOnly, ofShareAware, ofEnableSizing]
    Title = 'Please select Advantage Server Database file'
    Left = 212
    Top = 188
  end
  object dsTabs: TDataSource
    DataSet = qryTabs
    OnDataChange = dsTabsDataChange
    OnUpdateData = dsTabsUpdateData
    Left = 180
    Top = 188
  end
  object qryTabs: TAdsQuery
    DatabaseName = 'con'
    AdsTableOptions.AdsIndexPageSize = 512
    AdsTableOptions.AdsCachingOption = tcWrites
    SQL.Strings = (
      'select'
      '  t.*,'
      '  IIF(t.table_name is null,0,1) Exist'
      'from ('
      '  select substring(name,1,50) as "Table",'
      
        '    (select Trig_Container from system.triggers tr where trig_ta' +
        'blename=t.name and Trig_Event_Type=1 and tr.name='#39'ti_sync_'#39'+t.na' +
        'me) as i_s,'
      
        '    (select Trig_Container from system.triggers tr where trig_ta' +
        'blename=t.name and Trig_Event_Type=2 and tr.name='#39'tu_sync_'#39'+t.na' +
        'me) as u_s,'
      
        '    (select Trig_Container from system.triggers tr where trig_ta' +
        'blename=t.name and Trig_Event_Type=3 and tr.name='#39'td_sync_'#39'+t.na' +
        'me) as d_s,'
      #9'st.*'
      'from '
      
        '    system.tables t LEFT JOIN sync_settings st on st.table_name=' +
        't.name'
      'where'
      
        '  t.name not in ('#39'sync'#39', '#39'sync_comp'#39', '#39'sync_msg'#39', '#39'sync_settings' +
        #39') '
      ') as t'
      'order by t."Table"')
    AdsConnection = con
    Left = 116
    Top = 188
    ParamData = <>
    object qryTabsTable: TAdsStringField
      FieldName = 'Table'
      Size = 50
    end
    object qryTabsi_s: TWideMemoField
      FieldName = 'i_s'
      BlobType = ftWideMemo
      Size = 1
    end
    object qryTabsu_s: TWideMemoField
      FieldName = 'u_s'
      BlobType = ftWideMemo
      Size = 1
    end
    object qryTabsd_s: TWideMemoField
      FieldName = 'd_s'
      BlobType = ftWideMemo
      Size = 1
    end
    object qryTabstable_name: TAdsStringField
      FieldName = 'table_name'
      Size = 50
    end
    object qryTabskeys: TAdsStringField
      FieldName = 'keys'
      Size = 100
    end
    object qryTabsins: TIntegerField
      FieldName = 'ins'
    end
    object qryTabsins_exc: TIntegerField
      FieldName = 'ins_exc'
    end
    object qryTabsins_fields: TAdsStringField
      FieldName = 'ins_fields'
      Size = 15000
    end
    object qryTabsupd: TIntegerField
      FieldName = 'upd'
    end
    object qryTabsupd_exc: TIntegerField
      FieldName = 'upd_exc'
    end
    object qryTabsupd_fields: TAdsStringField
      FieldName = 'upd_fields'
      Size = 15000
    end
    object qryTabsdel: TIntegerField
      FieldName = 'del'
    end
    object qryTabsdel_exc: TIntegerField
      FieldName = 'del_exc'
    end
    object qryTabsdel_fields: TAdsStringField
      FieldName = 'del_fields'
      Size = 15000
    end
    object qryTabsExist: TIntegerField
      FieldName = 'Exist'
    end
  end
  object dlgSave: TSaveDialog
    DefaultExt = 'sql'
    Filter = 'sql|*.sql'
    Left = 244
    Top = 188
  end
end
