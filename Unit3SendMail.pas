unit Unit3SendMail;

interface

uses
  Windows, SysUtils, Classes, IdMessage, IdIOHandler, IdIOHandlerSocket, IdIOHandlerStack, IdSSL,
  IdSSLOpenSSL,
  IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient, IdExplicitTLSClientServerBase,
  IdMessageClient,
  IdSMTPBase, IdSMTP;

type
  TdmSendEmail = class(TDataModule)
    IdSMTP: TIdSMTP;
    IdSSLIOHandlerSocketOpenSSL: TIdSSLIOHandlerSocketOpenSSL;
    IdMessage: TIdMessage;
  private
    { Private declarations }
  public
    procedure SendMessage(const ASubject, AMessage: string);
  end;

var
  dmSendEmail: TdmSendEmail;

implementation

uses log;
{$R *.dfm}

function IndyEmailDlls: Boolean;
var
  HDll: THandle;
begin
  HDll := LoadLibrary('libeay32.dll');
  Result := HDll >= 32;
  if Result then
    FreeLibrary(HDll);

  if Result then
  begin
    HDll := LoadLibrary('ssleay32.dll');
    Result := HDll >= 32;
    if Result then
      FreeLibrary(HDll);
  end;
end;

{ TdmSendEmail }

procedure TdmSendEmail.SendMessage(const ASubject, AMessage: string);
begin
  IdMessage.From.Address := 'cdsync@compudime.com';
  IdMessage.From.Name := 'CompuDime Sync Reports';
  IdMessage.Subject := ASubject;
  IdMessage.Body.Text := AMessage;
  IdMessage.Recipients.Clear;
  with IdMessage.Recipients.Add do
    Address := 'cdsync@compudime.com';

  WriteToLog(Format('Sending Email Subject: %s, Message: %s', [ASubject, AMessage]), 'err.log');
  if IndyEmailDlls then
  begin
    IdSMTP.Connect;
    if IdSMTP.Connected then
    begin
      try
        try
          IdSMTP.Send(IdMessage);
        except
          on e: Exception do
            WriteToLog(Format('Email error: %s', [e.Message]), 'err.log');
        end;
      finally
        IdSMTP.Disconnect;
      end;
    end;
  end;
end;

end.
