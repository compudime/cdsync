unit cdsyn_testbed;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, adsdata, adsfunc, adstable, adscnnct, Grids, DBGrids, StdCtrls, math,
  Spin, ExtCtrls;

type
  TForm3 = class(TForm)
    btn1: TButton;
    edt1: TEdit;
    dlgOpen1: TOpenDialog;
    dbgrd1: TDBGrid;
    tbl1: TAdsTable;
    btn2: TButton;
    ds1: TDataSource;
    lbl1: TLabel;
    SpinEdit1: TSpinEdit;
    SpinEdit2: TSpinEdit;
    lbl2: TLabel;
    lbl3: TLabel;
    lbl4: TLabel;
    edt2: TEdit;
    lbl5: TLabel;
    tbl2: TAdsTable;
    ds2: TDataSource;
    dbgrd2: TDBGrid;
    lbl6: TLabel;
    lbl7: TLabel;
    tbl3: TAdsTable;
    tbl4: TAdsTable;
    ds3: TDataSource;
    ds4: TDataSource;
    dbgrd3: TDBGrid;
    dbgrd4: TDBGrid;
    lbl8: TLabel;
    AdsConnection1: TAdsConnection;
    tmr1: TTimer;
    procedure btn2Click(Sender: TObject);
    procedure btn1Click(Sender: TObject);
    procedure tbl1AfterOpen(DataSet: TDataSet);
    procedure tmr1Timer(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form3: TForm3;
  Xrun, tbl_run, AmountToRun: Integer;

implementation

{$R *.dfm}

procedure TForm3.btn1Click(Sender: TObject);
begin
  if btn1.Tag = 0 then
  begin
    btn1.Tag := 1;
    btn1.Caption := 'Stop';
    Xrun := 0;
  end
  else
  begin
    btn1.Tag := 0;
    btn1.Caption := 'Start';
  end;

  tbl1.First;
  tbl_run := 1;
  while (Xrun < SpinEdit1.Value) and (btn1.Tag = 1) do
  begin
    Xrun := Xrun + 1;
    tbl1.Edit;
    tbl1.FieldByName('desc').AsString := IntToStr(tbl1.RecNo) + ' - ' + IntToStr(tbl_run) + ' ' + edt2.Text;

    tbl1.Post;
    tbl1.Next;
    lbl1.Caption := IntToStr(Xrun);
    lbl1.Refresh;
    Application.ProcessMessages;
    Sleep(SpinEdit2.Value);
    if tbl1.Eof then
    begin
      tbl1.First;
      tbl_run := tbl_run + 1;
    end;

  end;
  btn1.Tag := 0;
  btn1.Caption := 'Start';

end;

procedure TForm3.btn2Click(Sender: TObject);
begin
  if dlgOpen1.Execute() then
  begin
    tbl1.Active := False;
    tbl2.Active := False;
    tbl3.Active := False;
    tbl4.Active := False;
    edt1.Text := dlgOpen1.FileName;
    AdsConnection1.ConnectPath := edt1.Text;
    tbl1.Active := True;
    tbl2.Active := True;
    tbl3.Active := True;
    tbl4.Active := True;
  end;
end;

procedure TForm3.tbl1AfterOpen(DataSet: TDataSet);
begin
  btn1.Enabled := True;
end;

procedure TForm3.tmr1Timer(Sender: TObject);
begin
  if not tbl4.Active then
    Exit;

  tbl2.Refresh;
  tbl2.Last;
  tbl3.Refresh;
  tbl3.Last;
  tbl4.Last;
  tbl4.Refresh;
  lbl6.Caption := 'Sync - ' + IntToStr(tbl2.RecordCount);
  lbl7.Caption := 'Sync_Msg - ' + IntToStr(tbl3.RecordCount);
  lbl8.Caption := 'Sync_Comp - ' + IntToStr(tbl4.RecordCount);

end;

end.
