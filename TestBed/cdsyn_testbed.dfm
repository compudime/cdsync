object Form3: TForm3
  Left = 0
  Top = 0
  Caption = 'Form3'
  ClientHeight = 550
  ClientWidth = 1242
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object lbl1: TLabel
    Left = 734
    Top = 350
    Width = 3
    Height = 13
  end
  object lbl2: TLabel
    Left = 105
    Top = 350
    Width = 16
    Height = 13
    Caption = 'File'
  end
  object lbl3: TLabel
    Left = 304
    Top = 350
    Width = 29
    Height = 13
    Caption = 'Count'
  end
  object lbl4: TLabel
    Left = 489
    Top = 350
    Width = 27
    Height = 13
    Caption = 'Delay'
  end
  object lbl5: TLabel
    Left = 600
    Top = 350
    Width = 28
    Height = 13
    Caption = 'Suffix'
  end
  object lbl6: TLabel
    Left = 8
    Top = 508
    Width = 26
    Height = 13
    Caption = 'Sync '
  end
  object lbl7: TLabel
    Left = 348
    Top = 505
    Width = 48
    Height = 13
    Caption = 'Sync_msg'
  end
  object lbl8: TLabel
    Left = 679
    Top = 508
    Width = 54
    Height = 13
    Caption = 'Sync_comp'
  end
  object btn1: TButton
    Left = 743
    Top = 316
    Width = 75
    Height = 25
    Caption = 'Start'
    Enabled = False
    TabOrder = 0
    OnClick = btn1Click
  end
  object edt1: TEdit
    Left = 105
    Top = 323
    Width = 183
    Height = 21
    TabOrder = 1
  end
  object dbgrd1: TDBGrid
    Left = -1
    Top = 19
    Width = 992
    Height = 291
    DataSource = ds1
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object btn2: TButton
    Left = 8
    Top = 316
    Width = 75
    Height = 25
    Caption = 'File'
    TabOrder = 3
    OnClick = btn2Click
  end
  object SpinEdit1: TSpinEdit
    Left = 304
    Top = 322
    Width = 162
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 4
    Value = 0
  end
  object SpinEdit2: TSpinEdit
    Left = 489
    Top = 318
    Width = 84
    Height = 22
    Hint = 'Slip'
    MaxValue = 0
    MinValue = 0
    TabOrder = 5
    Value = 0
  end
  object edt2: TEdit
    Left = 600
    Top = 318
    Width = 121
    Height = 21
    TabOrder = 6
  end
  object dbgrd2: TDBGrid
    Left = 8
    Top = 369
    Width = 312
    Height = 133
    DataSource = ds2
    TabOrder = 7
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object dbgrd3: TDBGrid
    Left = 339
    Top = 366
    Width = 312
    Height = 133
    DataSource = ds3
    TabOrder = 8
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object dbgrd4: TDBGrid
    Left = 679
    Top = 369
    Width = 312
    Height = 133
    DataSource = ds4
    TabOrder = 9
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object dlgOpen1: TOpenDialog
    DefaultExt = '*.add'
    Filter = 'Data Dict|*.add'
    Options = [ofEnableSizing]
    Left = 186
    Top = 33
  end
  object tbl1: TAdsTable
    AutoCalcFields = False
    StoreActive = False
    AfterOpen = tbl1AfterOpen
    AdsConnection = AdsConnection1
    AdsTableOptions.AdsIndexPageSize = 1024
    AdsTableOptions.AdsCalcFieldsBeforeFilter = True
    TableName = 'PRDMSTR'
    Left = 84
    Top = 21
  end
  object ds1: TDataSource
    DataSet = tbl1
    Left = 131
    Top = 24
  end
  object tbl2: TAdsTable
    StoreActive = False
    AdsConnection = AdsConnection1
    AdsTableOptions.AdsRightsCheck = False
    AdsTableOptions.AdsCalcFieldsBeforeFilter = True
    TableName = 'Sync'
    Left = 84
    Top = 74
  end
  object ds2: TDataSource
    DataSet = tbl2
    Left = 126
    Top = 78
  end
  object tbl3: TAdsTable
    StoreActive = False
    AdsConnection = AdsConnection1
    TableName = 'Sync_msg'
    Left = 84
    Top = 134
  end
  object tbl4: TAdsTable
    StoreActive = False
    AdsConnection = AdsConnection1
    TableName = 'Sync_comp'
    Left = 83
    Top = 198
  end
  object ds3: TDataSource
    DataSet = tbl3
    Left = 126
    Top = 133
  end
  object ds4: TDataSource
    DataSet = tbl4
    Left = 131
    Top = 200
  end
  object AdsConnection1: TAdsConnection
    AdsServerTypes = [stADS_REMOTE, stADS_LOCAL]
    LoginPrompt = False
    StoreConnected = False
    CancelOnRollback = False
    Left = 202
    Top = 107
  end
  object tmr1: TTimer
    OnTimer = tmr1Timer
    Left = 334
    Top = 158
  end
end
