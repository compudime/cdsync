sc query type= service state= all | find "CDSyncClient" >> SERVICECHECK.LOG 2>>&1
if not .%errorlevel%.==.0. goto EXIT
net stop CDSyncClient
CDSyncClient.exe /uninstall
:EXIT