# CDSyncSetup


## Reading Triggers
Sql query for the grid, The results of this query populate the grid that is displayed to the user.
``` 
select
  t.*,
  IIF(t.table_name is null,0,1) Exist
from (
  select substring(name,1,50) as "Table",
    (select Trig_Container from system.triggers tr where trig_tablename=t.name and Trig_Event_Type=1 and tr.name='ti_sync_'+t.name) as i_s,
    (select Trig_Container from system.triggers tr where trig_tablename=t.name and Trig_Event_Type=2 and tr.name='tu_sync_'+t.name) as u_s,
    (select Trig_Container from system.triggers tr where trig_tablename=t.name and Trig_Event_Type=3 and tr.name='td_sync_'+t.name) as d_s,
	st.*
from 
    system.tables t LEFT JOIN sync_settings st on st.table_name=t.name
where
  t.name not in ('sync', 'sync_comp', 'sync_msg', 'sync_settings') 
) as t
order by t."Table"
```
Tables:
- System.tables
- System.triggers
- sync_settings

## WARNING NOTE when interacting with the grid
> clicking on any of the checkboxs in the grid will delete the triggers asscociated with the table.

## Creating Triggers
if you want to create a trigger, select the primary key in the key field and then select one of the insert, update or/and delete check boxs. A trigger will be automatically created 

function that creates the triggers

``` 
/// <summary>
/// Creates a trigger based on the specified type (ins,upd,del).
/// </summary>
/// <param name="sType">The type of trigger to create.</param>
procedure TfrmRepl.create_trig(sType: string = '');
...
```

### The actual trigger construction happens here
Note the trigger templates are read from resource files

```
  {in order to create the triggers we seem to read the template from resource files}
  case sType[1] of
    'i':
      begin
        sCols := GetSQLValue(ReadTemplateRes('sel_cols_sql', [':tab', tabName, ':cols_restrict', sColsRestr]), '', ',');
        iVals := GetSQLValue(ReadTemplateRes('ins_vals_sql', [':tab', tabName, ':cols_restrict', sColsRestr]));
        Sql := ReadTemplateRes('ins_trg', [':tab', tabName, ':cols', sCols, ':sel_vals', iVals, ':WhereKey',
          uWhereKey]);
      end;
    'u':
      begin
        // this is my starting point
        uSelFields := GetSQLValue(ReadTemplateRes('upd_sel_flds_sql', [':tab', tabName, ':cols_restrict', sColsRestr]));
        Sql := ReadTemplateRes('upd_trg', [':tab', tabName, ':WhereKey', uWhereKey, ':SelFields', uSelFields]);
      end;
    'd':
      Sql := ReadTemplateRes('del_trg', [':tab', tabName, ':WhereKey', uWhereKey]);
  end;
```

## Custom Triggers
if a trigger follows the naming format found in the selecting sql statement
```
...
 and Trig_Event_Type=1 and tr.name='ti_sync_'+t.name,
 ...
 and Trig_Event_Type=2 and tr.name='tu_sync_'+t.name,
 ...
 and Trig_Event_Type=3 and tr.name='td_sync_'+t.name
```
but does not have any keys associated with the trigger table name 
in the sync_settings table the program will read in the trigger
and you will be able read in the keys and columns from that trigger assuming the program cannot read in the format


### Sql Statement for clearing a particular tables info
```
UPDATE SYNC_SETTINGS
SET KEYS = '',
upd_fields = '',
ins_fields = ''
WHERE TABLE_NAME = [Table Name];
```

```
{
  // these are the types I know the names for
Advantage Data Types
  1 : Logical
  2 : Numeric
  3 : DATE
  4 :
  5 :
  6 : Binary
  7 : Image
  8 : Varchar
  9 :
 10 : Double
 11 : Integer
 12 : Shortint
 13 : Time
 14 :
 15 :
 16 :
 17 :
 18 :
 19 :
 20 :
 21 :
 22 :
 23 : varcharfox
 28 : nMemo
 29 : Guid
}

// these are all the supported types mapped to default values
  zero = '0';
  emptyString = '''''';
  date = 'CAST(''1900-01-01'' AS SQL_DATE)';
  dateTime = 'CAST(''1900-01-01 00:00:00'' AS SQL_TIMESTAMP)';
  time = 'CAST(''00:00:00'' AS SQL_TIME)';

  1, zero
  2, zero
  3, date
  4, emptyString
  5, emptyString
  6, zero
  7, zero
  8, emptyString
  10, zero
  11, zero
  12, zero
  13, time
  14, dateTime
  18, zero
  20, emptyString
  23, emptyString
  26, emptyString
  28, emptyString
  29, emptyString

```