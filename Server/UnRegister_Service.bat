sc query type= service state= all | find "CDSyncServ" >> SERVICECHECK.LOG 2>>&1
if not .%errorlevel%.==.0. goto EXIT
net stop CDSyncServ
CDSyncServ.exe /uninstall
:EXIT