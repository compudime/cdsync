CREATE TRIGGER tu_sync_:tab ON :tab
AFTER UPDATE
BEGIN

Declare @Script String, @WhereClause String;
SET @Script = '';
SET @WhereClause = ' where'+ :WhereKey + ';';

:SelFields

IF @Script <> '' THEN
--  remove last comma
  @Script = SubString(@Script, 1, length(@Script) - 1); 
  insert into sync(EXPR, TableName, CallType) VALUES('update :tab set '+ @Script + @WhereClause, ':tab', 'UPDATE'); 
END IF; 
END;