create trigger td_syncc
on sync_comp
after delete 
begin
  delete from sync_msg where comp in (select comp from __old);
end;