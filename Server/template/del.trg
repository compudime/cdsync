create trigger td_sync_:tab
on :tab
after delete 
begin
  insert into sync(EXPR, TableName, CallType)
  select 'delete from :tab where '+:WhereKey,':tab','DELETE' from __old;
end;
