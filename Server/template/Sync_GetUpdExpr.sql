CREATE FUNCTION Sync_GetUpdExpr 
   (FldName CHAR ( 15000 ), oVal CHAR ( 15000 ), nVal CHAR ( 15000 ), FieldType INTEGER)
   RETURNS CHAR ( 15000 )
BEGIN
DECLARE @sTmp  CHAR(15000);   
if FieldType=5 or FieldType=4 or FieldType=3 or FieldType=20 or FieldType=5 then
  @sTmp=''''+Replace(substring(nVal,1,length(nVal)),'''','''''')+''',';   
else     
  @sTmp=substring(nVal,1,length(nVal))+',';   
end if;   
Set @sTmp=substring(@sTmp,1,length(@sTmp));   
Set @sTmp=IIF((nVal is null and oVal is null) or (nVal=oVal) ,'',substring(FldName,1,length(FldName)+1)+'='+IIF(nVal is null,'NULL,',@sTmp));   
RETURN substring(@sTmp,1,length(@sTmp)); 
END;
