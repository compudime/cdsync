create trigger ti_sync_:tab
on :tab
after insert
begin
  insert into sync(EXPR, TableName, CallType)
  select
    'MERGE :tab ON ('+:WhereKey+')
    WHEN NOT MATCHED THEN insert(:cols) values('+
    SUBSTRING(insVals,1,length(insVals)-1)+')', ':tab', 'INSERT'
  from (
    select
      n.*,
      :sel_vals'' as insVals
    from __new as n
  ) as tab where insVals<>'';
end;
