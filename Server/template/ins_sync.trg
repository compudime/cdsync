create trigger ti_sync
on sync
after insert
begin
  insert into sync_msg(id,comp) 
  select n.id,c.comp from __new as n, sync_comp as c;
end;
