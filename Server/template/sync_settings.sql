CREATE TABLE sync_settings(
  table_name CIChar(50) CONSTRAINT NOT NULL,
  keys CIChar(100) CONSTRAINT NOT NULL default '',
  ins integer CONSTRAINT NOT NULL default '0' , 
  ins_exc integer CONSTRAINT NOT NULL  default '0',
  ins_fields CIChar(15000) CONSTRAINT NOT NULL default '',
  upd integer CONSTRAINT NOT NULL default '0' , 
  upd_exc integer CONSTRAINT NOT NULL  default '0',
  upd_fields CIChar(15000) CONSTRAINT NOT NULL default '',
  del integer CONSTRAINT NOT NULL default '0' , 
  del_exc integer CONSTRAINT NOT NULL  default '0',
  del_fields CIChar(15000) CONSTRAINT NOT NULL default ''
  ) IN DATABASE;