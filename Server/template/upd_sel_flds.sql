select
 Replace(
    Replace('trim(Sync_GetUpdExpr('':name'',:ZEROFIXCONVERT(o.:name,SQL_VARCHAR),:ZEROFIXCONVERT(n.:name,SQL_VARCHAR),'+CONVERT(Field_Type,SQL_VARCHAR)+'))'
         ,':ZEROFIX',IIF(Field_Type in (2,10,11,18),'''0''+','')
    ),
 ':name','['+name+']')+'+'
from 
  system.columns 
where 
  parent=':tab' and name in (:cols_restrict)