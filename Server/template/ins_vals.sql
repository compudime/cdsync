select
  Replace(
      Replace(Replace('IIF(:name is null,''NULL,'','''':KAV+:ZEROFIXCONVERT(:name,SQL_VARCHAR)+:KAV'','')',
       ':KAV', IIF(Field_Type in (1,2,10,11,18),'','''''')),':ZEROFIX',IIF(Field_Type in (2,10,11,18),'''0''+','')),
   ':name',IIF(Field_Type=4,'Replace(n.['+name+'],'''''''','''''''''''')','n.['+name+']'))+'+'
from 
  system.columns
where 
  parent=':tab' and name in (:cols_restrict)