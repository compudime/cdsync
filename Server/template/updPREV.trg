create trigger tu_sync_:tab on :tab after update
begin
  Insert Into Sync(EXPR, TableName, CallType)
  Select 'Update :tab Set ' + SubString(updSet,1,length(updSet)-1) + ' Where ' + :WhereKey, ':tab', 'UPDATE'
  From (
    Select
      o.*,
      :SelFields''
     As updSet
    From (Select __new.* From __new) As n, (Select __old.* From __old) As o
  ) As tab Where updSet<>'';
end;