create trigger td_syncm
on sync_msg
after delete 
begin
  delete from sync where id in (select id from __old) and (select count(1) from sync_msg where id in (select id from __old))=0;
end;