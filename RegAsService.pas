unit RegAsService;

interface

uses
  Windows, SysUtils, SvcMgr, Classes;

type
  TCDReplicationService = class(TService)
    procedure ServiceCreate(Sender: TObject);
    procedure ServiceAfterInstall(Sender: TService);
    procedure ServiceBeforeInstall(Sender: TService);
  private
    procedure ServiceLoadName;
  public
    function GetServiceController: TServiceController; override;
  end;

var
  CDReplicationService: TCDReplicationService;

implementation

uses
  Registry, System.IniFiles;

{$R *.DFM}

procedure ServiceController(CtrlCode: DWord); stdcall;
begin
  CDReplicationService.Controller(CtrlCode);
end;

function TCDReplicationService.GetServiceController: TServiceController;
begin
  Result := ServiceController;
end;

procedure TCDReplicationService.ServiceAfterInstall(Sender: TService);
var
  Reg: TRegistry;
begin
  Reg := TRegistry.Create(KEY_READ or KEY_WRITE);
  try
    Reg.RootKey := HKEY_LOCAL_MACHINE;
    if Reg.OpenKey('\SYSTEM\CurrentControlSet\Services\' + Name, false) then
    begin
{$IFDEF SYRCLIENT}
      Reg.WriteString('Description', 'CompuDime Data Synchronization Client');
{$ELSE}
      Reg.WriteString('Description', 'CompuDime Data Synchronization Server ');
{$ENDIF}
      Reg.WriteInteger('DelayedAutostart', 1);
      Reg.CloseKey;
    end;
  finally
    Reg.Free;
  end;
end;

procedure TCDReplicationService.ServiceBeforeInstall(Sender: TService);
begin
  ServiceLoadName;
end;

procedure TCDReplicationService.ServiceCreate(Sender: TObject);
begin
  ServiceLoadName;
end;

procedure TCDReplicationService.ServiceLoadName;
var
  AppName, AAddName: string;
begin
  AppName := ExtractFileName(ParamStr(0));
{$IFDEF SYRCLIENT}
  Name := 'CDSyncClient';
{$ELSE}
  Name := 'CDSyncServ';
{$ENDIF}

  with TIniFile.Create(ChangeFileExt(ParamStr(0), '.ini')) do
  begin
    AAddName := ReadString('ServiceName', 'Suffix', '');
    Free;
  end;
  if AAddName<>'' then
    Name := Name + AAddName;

  DisplayName := Name;
end;

end.
