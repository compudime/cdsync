unit Unit1;

// IMPORTANT: search for "Customization parameters"
// to set them. How it works is also explained along
// with those parameters.

{$define NewWorkerThreads}
// Define LOCALTEST is used for testing with special TestClient
// program only, as given in the Docs.

// Minor syntax difference for newer version of OmniLib
{$define NewOmniLib}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, Dialogs, AdsCnnct, DB, AdsData, AdsFunc,
  AdsTable, StdCtrls, ShellApi, ComObj, IdBaseComponent, IdComponent, IdUDPBase, IdUDPClient, IdUDPServer,
  IdSocketHandle, IdTCPConnection, IdTCPClient, IdTCPServer, WinSock, ExtCtrls, Mutex, IdCustomTCPServer, IDContext,
  Spin, OtlEventMonitor, OtlTaskControl, OtlTask, OtlCommon, OtlComm, IdAntiFreezeBase, IdAntiFreeze,
  Generics.Collections, OtlParallel, OtlThreadPool;

{
  ----------------------------
  Customization parameters START
  Important: Control parameters, New:
  Set the following to a suitable value.
  Also read notes on how it works here.
}
const
  // SO MANY WORKERS, one for each client will be running
  // at one time. Those not running will be waiting in the
  // queue.
  WORKER_MAX_THREADS: Integer = 5;

  // A worker task fetches SO MANY UPDATES from DB for its
  // client and keeps them in memory, sending them
  // one by one in its processing loop. When list is finished,
  // it stops and puts itself again at the end of queue to
  // process after other workers are done.
  WORKER_NUM_UPDATES_IN_MEMORY = 10;

  // The time in msecs for processing loop of the worker.
  // A worker keeps a list of so many as above updates to
  // send, checking previous and sending next each time.
  WORKER_WAKEUP_DELAY_MSECS = 50;

  { Customization parameters END
    ----------------------------
  }

const
  WM_EXECSQL = WM_USER + 1313;

  // Constants used by new logic
  MSG_WAKESENDWORKER = WM_USER;
  MSG_GETCLIENTUPDATES = WM_USER + 1;
  MSG_CLIENTUPDATESRESULT = WM_USER + 2;
  MSG_SEND2CLIENTNEW_DONE = WM_USER + 3;
  MSG_SEND2CLIENTNEW_EXCEPT = WM_USER + 4;
  MSG_RESETBUSYINUPDATE = WM_USER + 5;

  // Terminate codes for worker task to identify
  // why it terminated
  WORKER_EXIT_LIST_DONE = 999;
  WORKER_EXIT_COMM_ERROR = 998;
  WORKER_EXIT_NO_UPDATES_FOUND = 997;
  WORKER_EXIT_REMOVE_COMMAND = 996;

type
  TStringListEx = class(TStringList)
  public
    function Find(const S: string; var Index: Integer): Boolean; override;
  end;

  TSendWorkerInfo = Record
    tpId: string;
    tpUpd: string;
    tpPort: Integer;
  end;

  TfrmRepl = class(TForm)
    qryExec: TAdsQuery;
    con: TAdsConnection;
    oDlg: TOpenDialog;
    client: TIdTCPClient;
    serv: TIdTCPServer;
    pnlBottom: TPanel;
    Panel2: TPanel;
    gbServer: TGroupBox;
    btnReg: TButton;
    gbtabs: TGroupBox;
    mTabs: TMemo;
    btnCreateTriggers: TButton;
    servTimer: TTimer;
    pnlServerMode: TPanel;
    chkAllowLocal: TCheckBox;
    tbSyncMsg: TAdsTable;
    qrySyncQuery: TAdsQuery;
    lblMinutesBusy: TLabel;
    seMinutesBusy: TSpinEdit;
    qryCheckTime: TAdsQuery;
    ClientBusy: TTimer;
    oem1: TOmniEventMonitor;
    IdAntiFreeze1: TIdAntiFreeze;
    qryProcess: TAdsQuery;
    tbSyncComp: TAdsTable;
    cleanupDownlistTimer: TTimer;
    oem2: TOmniEventMonitor;
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    btnCustomName: TButton;
    grpDbConnectionSet: TGroupBox;
    edCon: TEdit;
    btnSetCon: TButton;
    Button1: TButton;
    edtCustomName: TEdit;
    Label2: TLabel;
    edtPort: TEdit;
    pnlServerSet: TPanel;
    Label1: TLabel;
    edClientName: TEdit;
    edServer: TEdit;
    HealthTimer: TTimer;
    Button2: TButton;
    procedure btnCreateTriggersClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnSetConClick(Sender: TObject);
    procedure servExecute(AThread: TIdContext);
    procedure btnRegClick(Sender: TObject);
    procedure servTimerTimer(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure servException(AContext: TIdContext; AException: Exception);
    procedure servListenException(AThread: TIdListenerThread; AException: Exception);
    procedure servDisconnect(AContext: TIdContext);
    procedure ClientBusyTimer(Sender: TObject);
    procedure oem1TaskTerminated(const task: IOmniTaskControl);
    procedure oem1TaskMessage(const task: IOmniTaskControl; const msg: TOmniMessage);
    procedure FormDestroy(Sender: TObject);
    procedure cleanupDownlistTimerTimer(Sender: TObject);
    procedure oem2TaskMessage(const task: IOmniTaskControl; const msg: TOmniMessage);
    procedure oem2TaskTerminated(const task: IOmniTaskControl);
    procedure oem2PoolThreadCreated(const pool: IOmniThreadPool; threadID: Integer);
    procedure oem2PoolThreadDestroying(const pool: IOmniThreadPool; threadID: Integer);
    procedure oem2PoolThreadKilled(const pool: IOmniThreadPool; threadID: Integer);
    procedure oem2PoolWorkItemCompleted(const pool: IOmniThreadPool; taskID: Int64);
    procedure conAfterConnect(Sender: TObject);
    procedure btnCustomNameClick(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure HealthTimerTimer(Sender: TObject);
    {$IFNDEF SYRCLIENT}
    procedure Timer1Timer(Sender: TObject);
    {$ENDIF}
  private
    CustomReg: string;
    FSend2ClientTaskList: TList<IOmniTaskControl>;
    FLastTaskID: Integer;

    FInSendUpdatesNew: Boolean;
    FInServExecute: Boolean;
    FStartListenServer: Boolean;
    FLastTimeUpdateSent: TDateTime;
    FSkipList: TStringListEx;
    FCloseCommandActive: Boolean;

    FSendWorkerTaskList: TList<IOmniWorker>;

    function UserPass: string;
    // procedure KeepClientsBusy;
    procedure CreateSyncTimeData;
    procedure CheckBusyStatus;
{$IFNDEF SYRCLIENT}
    procedure UpdateSyncTimeData;
    procedure AddCompDown(sComp: string);
    procedure getClientList(clientList: TStringList);
    procedure startClientWorkers;
{$IFDEF UNUSED_TRIED}
    function isClientOnline(SClient: string): Boolean;
    procedure PrepareDownList;
{$ENDIF}
{$ELSE}
    procedure SendTimeDataReport;
{$ENDIF}
    procedure getUpdatesForClient(aclient: string; numUpdates: Integer; slist: TList<TSendWorkerInfo>);
    procedure LogPoolStatus;
    function ConnectPort: Integer;
    function DatabasePath: string;
    procedure CheckForNewAllFiles;
    procedure CheckForPrevRestartAll;
  public
    procedure ExecSqlSync(var msg: TMessage); message WM_EXECSQL;
    procedure ReadConnectionSettings;
    procedure AddComp(sComp: string);
    procedure RemoveComp(sComp: string);
    function SetNewConnection: string;
    function ExecSql(Sql: string; SepCommandsDelimiter: char = #0): variant;
    function GetSQLValue(SQLText: string; Delim4Fields: string = ';'; Delim4Recs: string = #13#10): string;
    procedure CreateTriggers(TabName: string; KeysAndFields: string);
    { procedure SendUpdates; }
    procedure SendUpdatesNew;
    function Send2Client(SClient, sId, sUpd: string; iPort: Integer): Integer;
    procedure ListenServer;
    procedure startWorker(aclient: string; clientIsGettingErrors: Boolean);
    procedure StartServTimer;
    function existsWorker(aclientId: string): Boolean;
    procedure removeWorker(aclientId: string; isRemoveCommand: Boolean = False);
    function GetClientCompName(AThread: TIdContext): string;
  end;

type
  TClientStatus = Record
    ip: string;
    busy: Boolean;
  end;

type
  TClientStatusList = array of TClientStatus;

type
  TSend2ClientTaskParam = Record
    tpTaskID: Integer;
    tpClient: string;
    tpId: string;
    tpUpd: string;
    tpPort: Integer;
  end;

const
  // Used in old logic, not in new where customization parameters
  // at the top are used.
  MAX_TASKS = 5; // max number of threads

  MSG_SEND2CLIENT_DONE = 1;
  MSG_SEND2CLIENT_EXCEPT = 2;
  CUSTOM_TERMINATOR = '|x|';

type
  TSendWorker = class(TOmniWorker)
  strict private
    fClientId: string;
    fUpdatesList: TList<TSendWorkerInfo>;
    fAskedForUpdates, fBusyInUpdate: Boolean;

    fClientInError: Boolean;

    procedure sendNextUpdate;
    procedure verifySidAndDeleteUpdate(sId: string);
    procedure Send2Client(tpId, tpUpd: string; tpPort: Integer);
  public
    Constructor Create(aclientId: string; clientGotError: Boolean = False); overload;
    Destructor Destroy; override;

    function Initialize: Boolean; override;
    function isForClient(aclientId: string): Boolean;
    procedure OnWakeMessage(var msg: TOmniMessage); message MSG_WAKESENDWORKER;
    procedure OnClientUpdatesResultMessage(var msg: TOmniMessage); message MSG_CLIENTUPDATESRESULT;
    procedure OnResetBusyInUpdate(var msg: TOmniMessage); message MSG_RESETBUSYINUPDATE;
  end;

const
  MUTEX_NAME = 'CDSyncQuery_36C61B14-F748-4F6A-9FC2-BD691776DF17';
  NullDate = -700000;

var
  frmRepl: TfrmRepl;
  FRestartAllTime: TDateTime;
  sSyncSql: string;
  sSyncExecSqlRet: variant;
  fMutex: TMutex;
  ClientStates: TClientStatusList;

function ClientInStates(ip: string): Integer;
function AddClientStatus(ip: string): Integer;
procedure SetBusyFlag(ip: string; busy: Boolean);
function IsClientBusy(ip: string): Boolean;

function IsDesktopMode: Boolean;

implementation

uses FilesWork, WinInfo, Utils, DateUtils, Log, JclStrings, StrUtils, JclSysInfo, Registry, Unit2, Vcl.FileCtrl,
{$IFDEF SYRCLIENT}Unit3SendMail, {$ENDIF} AdsSet, RegAsService, System.IniFiles;
{$R *.dfm}

function ClientInStates(ip: string): Integer;
var
  i: Integer;
begin
  Result := -1;
  for i := Low(ClientStates) to High(ClientStates) do
    if ClientStates[i].ip = ip then
      Result := i;
end;

function AddClientStatus(ip: string): Integer;
begin
  Result := ClientInStates(ip);
  if Result < 0 then
  begin
    SetLength(ClientStates, Length(ClientStates) + 1);
    ClientStates[High(ClientStates)].ip := ip;
    ClientStates[High(ClientStates)].busy := False;
    Result := High(ClientStates)
  end;
end;

procedure SetBusyFlag(ip: string; busy: Boolean);
var
  n: Integer;
begin
  n := ClientInStates(ip);
  if n < 0 then
    n := AddClientStatus(ip);
  ClientStates[n].busy := busy;
end;

function IsClientBusy(ip: string): Boolean;
var
  n: Integer;
begin
  n := ClientInStates(ip);
  if n < 0 then
    Result := False
  else
    Result := ClientStates[n].busy;
end;

function IsDesktopMode: Boolean;
begin
  Result := (ParamCount > 0) and ((ParamStr(1) = '/auto') or (ParamStr(1) = '/gui'));
end;

function DDCryPas: string;
var
  a, b, c: string;
begin
  Result := '$';
  b := 'Encryption';
  a := 'tiswin';
  c := '$';
  Result := Result + UpperCase(Copy(a, 1, 1)) + Copy(a, 2, 2);
  Result := Result + UpperCase(Copy(a, 4, 1)) + Copy(a, 5, 2) + '3' + c;
  c := '%';
  Result := Result + Copy(b, 1, 6) + UpperCase(Copy(b, 7, 4));
  Result := Result + c;
end;

function SetGlobalEnvironment(const Name, Value: string; const User: Boolean): Boolean;
resourcestring
  REG_MACHINE_LOCATION = 'System\CurrentControlSet\Control\Session Manager\Environment';
  REG_USER_LOCATION = 'Environment';
begin
  with TRegistry.Create do
    try
      if User then { User Environment Variable }
        Result := OpenKey(REG_USER_LOCATION, True)
      else { System Environment Variable }
      begin
        RootKey := HKEY_LOCAL_MACHINE;
        Result := OpenKey(REG_MACHINE_LOCATION, True);
      end;
      if Result then
      begin
        { Write Registry for Global Environment }
        WriteString(Name, Value);
        { Update Current Process Environment Variable }
        SetEnvironmentVariable(PChar(Name), PChar(Value));
        { Send Message To All Top Window for Refresh }
        (* DP - this is potentionally dangerous, despite it has know issues. If it is REALLY necessary to broadcast change notification,
          there should be another way. On 2 of my systems this causes program to "hang". So I commented it out for a moment, it is even possible
          that it causes your system to sop working after some time!
        *)
        // SendMessage(HWND_BROADCAST, WM_SETTINGCHANGE, 0, Integer
        // (PChar('Environment')));
      end;
    finally
      Free;
    end;
end; { SetGlobalEnvironment }

function ReadTemplateRes(fName: string; const ReplArr: array of variant): string;
var
  rs: TResourceStream;
  st: TStringList;
begin
  fName := UpperCase(fName);
  rs := TResourceStream.Create(HInstance, fName, RT_RCDATA);
  st := TStringList.Create;
  try
    st.LoadFromStream(rs);
    Result := ReplaceAll(st.Text, ReplArr);
  finally
    rs.Free;
    st.Free;
  end;
end;

procedure TfrmRepl.FormCreate(Sender: TObject);
var
  AAddName: string;
begin
  with TIniFile.Create(ChangeFileExt(ParamStr(0), '.ini')) do
  begin
    AAddName := ReadString('ServiceName', 'Suffix', '');
    Free;
  end;
  edtCustomName.Text := AAddName;
{$IFDEF SYRCLIENT}
  CustomReg := 'Software\CDSyncClient' + AAddName;
{$ELSE}
  CustomReg := 'Software\CDSync' + AAddName;
{$ENDIF}
{$IFDEF SYRCLIENT}
  Self.Caption := Self.Caption + ' - Client';
  gbtabs.Visible := False;
  btnCreateTriggers.Visible := False;
  frmRepl.Height := frmRepl.Height - gbtabs.Height;
  edServer.Text := GetRegStringValue(CustomReg, 'Server', HKEY_LOCAL_MACHINE);
  edClientName.Text := GetRegStringValue(CustomReg, 'Client', HKEY_LOCAL_MACHINE);
  edtPort.Text := GetRegStringValue(CustomReg, 'Port', HKEY_LOCAL_MACHINE);
  serv.DefaultPort := ConnectPort;
  client.Port := ConnectPort + 1;
{$ELSE}
  // mTabs.Lines.LoadFromFile(AbsPath('tables.txt'));
  Self.Caption := Self.Caption + ' - Server';
  // gbServer.Visible := False;
  pnlServerSet.Visible := False;
  edtPort.Text := GetRegStringValue(CustomReg, 'Port', HKEY_LOCAL_MACHINE);
  client.Port := ConnectPort;
  serv.DefaultPort := ConnectPort + 1;
{$ENDIF}
  seMinutesBusy.Value := StrToIntDef(GetRegStringValue(CustomReg, 'MinutesBusy', HKEY_LOCAL_MACHINE), 0);
  FLastTimeUpdateSent := Now;
  ReadConnectionSettings;

  with TIniFile.Create(GetAliasPathAndFileName) do
  begin
    if not ReadBool('SETTINGS', 'RETRY_ADS_CONNECTS', False) then
      WriteBool('SETTINGS', 'RETRY_ADS_CONNECTS', True);
    Free;
  end;

  CheckForPrevRestartAll;
  CheckForNewAllFiles;

{$IFNDEF LOCALTEST}
  CreateSyncTimeData;
{$ENDIF}
{$IFNDEF SYRCLIENT}
  // Create new table, Sync_down, for down list needed for speed up logic
  ExecSql('DROP TABLE Sync_down; CREATE TABLE Sync_down(comp Char(250) CONSTRAINT PK_SYNC_DOWN PRIMARY KEY CONSTRAINT NOT NULL) IN DATABASE');
  ExecSql('EXECUTE PROCEDURE sp_ModifyTableProperty( ''Sync_down'', ''Table_Auto_Create'', ''True'', ''APPEND_FAIL'', ''SyncFail'')');
{$ENDIF}
  FSkipList := TStringListEx.Create;
  // fix: sort was missing at many ADD places
  FSkipList.Sorted := True;
  // fix: duplicate entries were added
  FSkipList.Duplicates := dupIgnore;

  FSend2ClientTaskList := TList<IOmniTaskControl>.Create;
  FSendWorkerTaskList := TList<IOmniWorker>.Create;
  FLastTaskID := 0;
end;

procedure TfrmRepl.FormDestroy(Sender: TObject);
begin
  FSkipList.Free;

  FSend2ClientTaskList.Free;

  FSendWorkerTaskList.Free;
end;

procedure TfrmRepl.FormShow(Sender: TObject);
begin
{$IFNDEF LOCALTEST}
  if PasswordDlg.ShowModal <> mrOK then
    Application.Terminate;
{$ENDIF}
end;

function TfrmRepl.ExecSql(Sql: string; SepCommandsDelimiter: char = #0): variant;
var
  qry: TAdsQuery;
  flQry: Boolean;
  vDescr, vRecs, vRec: variant;
  ArrRecs: array of variant;
  CurRec, i: Integer;
  lstCommands: TStringList;
  sValue: string;
begin
  // SendDebugEx(Application.ExeName+'::ExecSql: '+Sql, mtInformation);
  if GetCurrentThreadId <> MainThreadID then
  begin
    fMutex.Enter;
    try
      sSyncSql := Sql;
      SendMessage(Self.Handle, WM_EXECSQL, 0, 0);
      Result := sSyncExecSqlRet;
      Exit;
    finally
      fMutex.Leave;
    end;
  end;

  Result := 0;
  lstCommands := TStringList.Create;
  try
    if not con.IsConnected then
      con.IsConnected := True;

    //lstCommands.Text := Replace(Replace(Sql, #13#10, ' '), SepCommandsDelimiter, #13#10);
    //WriteToLog('Step 1: ' + IntToStr(Pos(#13#10, Sql)) + ' SQL: ' + Sql, 'err.log');
    Sql := ReplaceStr(Sql, #13#10, ' ');
    Sql := ReplaceStr(Sql, #10, ' ');
    //WriteToLog('Step 2: ' + IntToStr(Pos(SepCommandsDelimiter, Sql)) + ' SQL: ' + Sql, 'err.log');
    Sql := ReplaceStr(Sql, SepCommandsDelimiter, #13#10);
    //WriteToLog('Step 3: ' + Sql, 'err.log');
    lstCommands.Text := Sql;
    //WriteToLog('Step 4: ' + IntToStr(lstCommands.Count), 'err.log');

    flQry := (UpperCase(Copy(trim(Sql), 1, 6)) = 'SELECT') and (lstCommands.Count = 1);

    qry := qryExec;
    qry.Close;
    qry.Sql.Text := Sql;
    if not flQry then
      con.BeginTransaction;
    try
      if flQry then
      begin
        try
          WriteToLog('Before Open - 1 ' + qry.Sql.Text, 'trc.log');
          qry.Open;
          WriteToLog('After Open - 1', 'trc.log');
        except
          on e: Exception do
          begin
            if (Pos('NativeError = 5098', e.Message) > 0) and (Pos('Table name: ', e.Message) > 0) then
            begin
              sValue := Copy(e.Message, Pos('Table name: ', e.Message) + 12, 100);
              qry.AdsStmtSetTablePassword(sValue, DDCryPas);
              qry.Open;
            end
            else
              raise;
          end;
        end;

        Result := VarArrayCreate([0, 1], varVariant);
        vDescr := VarArrayCreate([0, qry.FieldCount - 1], varVariant);
        for i := 0 to qry.FieldCount - 1 do
          vDescr[i] := VarArrayOf([qry.Fields[i].FieldName, qry.Fields[i].DataType, qry.Fields[i].Size]);

        CurRec := 0;
        qry.First;
        while not qry.EOF do
        begin
          vRec := VarArrayCreate([0, qry.FieldCount - 1], varVariant);
          for i := 0 to qry.FieldCount - 1 do
            vRec[i] := qry.Fields[i].Value;
          SetLength(ArrRecs, CurRec + 1);
          ArrRecs[CurRec] := vRec;
          qry.Next;
          Inc(CurRec);
        end;
{$IFDEF FULLDEBUG}
        WriteToLog(Format('ExecSQL Record Count: %d', [CurRec]), 'debug.log');
{$ENDIF}
        vRecs := VarArrayCreate([0, High(ArrRecs)], varVariant);
        for i := 0 to High(ArrRecs) do
          vRecs[i] := ArrRecs[i];
        qry.Close;
        Result[0] := vDescr;
        Result[1] := vRecs;
      end
      else
      begin
        for i := 0 to lstCommands.Count - 1 do
        begin
          qry.Sql.Text := StringReplace(lstCommands[i], '/', '', []);
          if trim(qry.Sql.Text) <> '' then
            try
              WriteToLog('Before SQL - 1 ' + qry.Sql.Text, 'trc.log');
              qry.ExecSql;
              WriteToLog('After SQL - 2', 'trc.log');
            except
              on e: Exception do
              begin
                if (Pos('NativeError = 5098', e.Message) > 0) and (Pos('Table name: ', e.Message) > 0) then
                begin
                  sValue := Copy(e.Message, Pos('Table name: ', e.Message) + 12, 100);
                  qry.AdsStmtSetTablePassword(sValue, DDCryPas);
                  WriteToLog(Format('Encrypted Table: %s', [sValue]));
                  qry.ExecSql;
                end
                else
                begin
                  WriteToLog('Error SQL - 3 ' + e.Message, 'trc.log');
                  raise;
                end;
              end;
            end;
        end;
        con.Commit;
        Result := 1;
      end;
    except
      on e: Exception do
      begin
        Result := 'Exception : ' + e.Message + #13#10 + Sql;
        if Assigned(CDReplicationService) then
          CDReplicationService.LogMessage('Exception (1) : ' + e.Message + #13#10 + Sql);
        WriteToLog('Exception (1) : ' + e.Message + #13#10 + Sql, 'err.log');
        if not flQry then
          con.Rollback;
      end;
    end;
  finally
    lstCommands.Free;
    if not FInSendUpdatesNew then
    begin
      con.IsConnected := False;
    end;
  end;
end;

procedure TfrmRepl.CheckBusyStatus;
var
  EMinsElapsed: Extended;
begin
  if seMinutesBusy.Value > 0 then
  begin
    EMinsElapsed := (Now - FLastTimeUpdateSent) * 24 * 60;
    if EMinsElapsed >= seMinutesBusy.Value then
    begin
{$IFNDEF SYRCLIENT}
      // KeepClientsBusy;
      UpdateSyncTimeData;
{$ELSE}
      SendTimeDataReport;
{$ENDIF}
      FLastTimeUpdateSent := Now;
    end;
  end;
end;

function FileTimeToLocalDateTime(AFileTime: TFileTime): TDateTime;
var
  SystemTime, LocalTime: TSystemTime;
begin
  if not FileTimeToSystemTime(AFileTime, SystemTime) then
    RaiseLastOSError;
  if not SystemTimeToTzSpecificLocalTime(nil, SystemTime, LocalTime) then
    RaiseLastOSError;
  Result := SystemTimeToDateTime(LocalTime);
end;

procedure TfrmRepl.CheckForNewAllFiles;
var
  APath: string;
  fad: TWin32FileAttributeData;
  ADoDisconnect: Boolean;
begin
  APath := DatabasePath;

  ADoDisconnect := FileExists(APath + '\Restart.all');

  if ADoDisconnect then
  begin
    ADoDisconnect := FRestartAllTime = NullDate;
    GetFileAttributesEx(PChar(APath + '\Restart.all'), GetFileExInfoStandard, @fad);
    if not ADoDisconnect then
      ADoDisconnect := FRestartAllTime <> FileTimeToLocalDateTime(fad.ftLastWriteTime);
    FRestartAllTime := FileTimeToLocalDateTime(fad.ftLastWriteTime);
  end;

  if not ADoDisconnect then
    ADoDisconnect := FileExists(APath + '\Close.all') or FileExists(APath + '\Lock.all');

  FCloseCommandActive := ADoDisconnect;
end;

procedure TfrmRepl.CheckForPrevRestartAll;
var
  APath: string;
  fad: TWin32FileAttributeData;
begin
  APath := DatabasePath;
  FRestartAllTime := NullDate;

  if FileExists(APath + '\Restart.all') then
  begin
    GetFileAttributesEx(PChar(APath + '\Restart.all'), GetFileExInfoStandard, @fad);
    FRestartAllTime := FileTimeToLocalDateTime(fad.ftLastWriteTime);
  end;
end;

procedure TfrmRepl.cleanupDownlistTimerTimer(Sender: TObject);
begin
  // cleans every N minutes based on what is set in timer
  // For example, 10 minutes can be a good delay
  ExecSql('Try Delete From Sync_Down; Catch All End;');
  WriteToLog('Clearing sync_down table', 'appp.log');
end;

procedure TfrmRepl.ClientBusyTimer(Sender: TObject);
begin
{$IFDEF SYRCLIENT}
  WriteToLog('ClientBusyTimer, FInServExecute=' + IfThen(FInServExecute, 'True', 'False'), 'err.log');
  if FInServExecute then
    Exit;

  if FStartListenServer and (not serv.Active) and (not servTimer.Enabled) then
  begin
    WriteToLog('ClientBusyTimer, Setting servTimer On since FStartListenServer is set', 'err.log');
    servTimer.Enabled := True;
  end;

  CheckBusyStatus;
{$ELSE}
  ClientBusy.Enabled := False;
{$ENDIF}
end;

procedure TfrmRepl.conAfterConnect(Sender: TObject);
var
  ACon: TAdsConnection;
begin
  ACon := TAdsConnection(Sender);
  try
    ACon.Execute('Try Execute Procedure cdUpdateCrUseData(); Catch All End;');
  except
  end;
end;

function TfrmRepl.ConnectPort: Integer;
begin
  Result := StrToIntDef(edtPort.Text, 8760)
end;

procedure TfrmRepl.CreateSyncTimeData;
begin
{$IFNDEF SYRCLIENT}
  ExecSql('if (select count(*) from system.tables where name=''SyncTime'')=0 then ' +
    'Try CREATE TABLE SyncTime(ID Char(40) CONSTRAINT NOT NULL, Cnt Integer DEFAULT ''0'', ' +
    'Created Date DEFAULT ''DATE()'', Stamp ModTime) IN DATABASE; EXECUTE PROCEDURE sp_CreateIndex90' +
    '(''SyncTime'', ''SyncTime.adi'', ''Created'', ''Created'', '''', 2051, 512, ''''); ' +
    'EXECUTE PROCEDURE sp_ModifyTableProperty(''SyncTime'', ''Table_Auto_Create'', ''True'', ' +
    '''APPEND_FAIL'', ''SyncTimefail''); Catch All End; endif;');
  CreateTriggers('SyncTime', 'ID');
{$ELSE}
  ExecSql('Try CREATE TABLE SyncTime(ID Char(40), Cnt Integer, Created Date, Stamp ModTime); Catch All End; ' +
    'Try EXECUTE PROCEDURE sp_CreateIndex90(''SyncTime'', ''SyncTime.adi'', ''ID'', ''ID'', '''', 2051, 512, ''''); ' +
    'Catch All End;');
{$ENDIF}
end;

procedure TfrmRepl.CreateTriggers(TabName: string; KeysAndFields: string);
var
  sqlIns: string;
  sqlUpd: string;
  sqlDel: string;
  sCols, iVals, uSelFields, uWhereKey, keys, KeyFields, sFields, sColsRestr: string;
  lstSplit: TStringList;
begin
  lstSplit := TStringList.Create;
  lstSplit.Text := StringReplace(KeysAndFields, ';', #13#10, [rfReplaceAll]);
  KeyFields := StringReplace(lstSplit[0], #13#10, '', [rfReplaceAll]);
  if (lstSplit.Count < 2) or (trim(lstSplit[1]) = '') then
    sFields := GetSQLValue
      (Format('select name from system.columns where upper(parent)=''%s'' And Field_Type<>22 And Field_Type<>15 And Field_Type<>21',
      [TabName]), '', ',')
  else
    sFields := StringReplace(lstSplit[1], #13#10, '', [rfReplaceAll]);

  keys := '''' + ReplaceAll(KeyFields, [' ', '', ',', ''',''']) + '''';
  sColsRestr := '''' + ReplaceAll(sFields, [' ', '', ',', ''',''']) + '''';

  uWhereKey := GetSQLValue(ReadTemplateRes('upd_where_keys_sql', [':tab', TabName, ':keys', keys]));

  sCols := GetSQLValue(ReadTemplateRes('sel_cols_sql', [':tab', TabName, ':cols_restrict', sColsRestr]), '', ',');
  iVals := GetSQLValue(ReadTemplateRes('ins_vals_sql', [':tab', TabName, ':cols_restrict', sColsRestr]));
  sqlIns := ReadTemplateRes('ins_trg', [':tab', TabName, ':cols', sCols, ':sel_vals', iVals, ':WhereKey', uWhereKey]);
  // Save2File('ins.txt', sqlIns);
  ExecSql(sqlIns);

  uSelFields := GetSQLValue(ReadTemplateRes('upd_sel_flds_sql', [':tab', TabName, ':cols_restrict', sColsRestr]));
  sqlUpd := ReadTemplateRes('upd_trg', [':tab', TabName, ':WhereKey', uWhereKey, ':SelFields', uSelFields]);
  // Save2File('upd.txt', sqlUpd);
  ExecSql(sqlUpd);

  sqlDel := ReadTemplateRes('del_trg', [':tab', TabName, ':WhereKey', uWhereKey]);
  // Save2File('del.txt', sqlDel);
  try
    ExecSql(sqlDel);
  except
    // Ignore It...
  end;
end;

function TfrmRepl.DatabasePath: string;
var
  APos: Integer;
begin
  Result := con.GetConnectionPath;
  APos := Pos(':6262', Result);
  if APos > 0 then
    Result := Copy(Result, 1, APos - 1) + Copy(Result, APos + 5, Length(Result));
end;

procedure TfrmRepl.btnCreateTriggersClick(Sender: TObject);
var
  i: Integer;
begin
  CreateRegKey(CustomReg, 'DBPath', edCon.Text, HKEY_LOCAL_MACHINE);
  CreateRegKey(CustomReg, 'MinutesBusy', seMinutesBusy.Text, HKEY_LOCAL_MACHINE);
{$IFNDEF SYRCLIENT}
  CreateRegKey(CustomReg, 'UseALS', BoolToStr(chkAllowLocal.Checked), HKEY_LOCAL_MACHINE);
{$ENDIF}
  ReadConnectionSettings;
{$IFDEF SYRCLIENT}
  Exit;
{$ENDIF}
  // mTabs.Lines.SaveToFile('tables.txt');

  ExecSql(GetSQLValue
    ('select ''drop trigger ''+trig_tablename+''.''+name+''/'' from system.triggers where name like ''t__sync__%'''),
    '/');

  ExecSql('CREATE TABLE Sync (ID AutoInc CONSTRAINT PK_SYNC PRIMARY KEY CONSTRAINT NOT NULL,EXPR Char(15000),TableName CIChar(50),CallType CIChar(15),DateTimeStmp ModTime) IN DATABASE');
  ExecSql('Alter Table Sync Add Column TableName CIChar( 50 ) Add Column CallType CIChar( 15 ) Add Column DateTimeStmp ModTime;');
  ExecSql('EXECUTE PROCEDURE sp_ModifyTableProperty( ''Sync'', ''Table_Auto_Create'', ''True'', ''APPEND_FAIL'', ''SyncFail'')');
  ExecSql('CREATE TABLE sync_msg(ID integer CONSTRAINT NOT NULL,comp Char(250) CONSTRAINT NOT NULL) IN DATABASE');
  ExecSql('CREATE INDEX IDX_SYNC_MSG_ID ON SYNC_MSG(ID)');
  ExecSql('CREATE INDEX IDX_SYNC_MSG_COMP ON SYNC_MSG(COMP)');
  ExecSql('EXECUTE PROCEDURE sp_ModifyTableProperty( ''Sync_Msg'', ''Table_Auto_Create'', ''True'', ''APPEND_FAIL'', ''SyncFail'')');
  ExecSql('CREATE TABLE Sync_Comp(comp Char(250) CONSTRAINT PK_SYNC_COMP PRIMARY KEY CONSTRAINT NOT NULL,lastsync TimeStamp default ''Now()'') IN DATABASE');
  ExecSql('EXECUTE PROCEDURE sp_ModifyTableProperty( ''Sync_Comp'', ''Table_Auto_Create'', ''True'', ''APPEND_FAIL'', ''SyncFail'')');
  ExecSql(ReadTemplateRes('ins_sync_trg', []));
  ExecSql(ReadTemplateRes('del_sync_comp_trg', []));
  ExecSql(ReadTemplateRes('del_sync_msg_trg', []));
  ExecSql(ReadTemplateRes('Sync_GetUpdExpr_sql', []));

  for i := 0 to mTabs.Lines.Count - 1 do
    CreateTriggers(mTabs.Lines.Names[i], mTabs.Lines.ValueFromIndex[i]);

  ShowMessage('Triggers Successfully Created.');
end;

procedure TfrmRepl.btnCustomNameClick(Sender: TObject);
begin
  if edtCustomName.Text <> '' then
  begin
    with TIniFile.Create(ChangeFileExt(ParamStr(0), '.ini')) do
    begin
      WriteString('ServiceName', 'Suffix', edtCustomName.Text);
      Free;
    end;
    ShowMessage('Suffix set to ' + edtCustomName.Text);
  end;
end;

function TfrmRepl.GetSQLValue(SQLText: string; Delim4Fields: string = ';'; Delim4Recs: string = #13#10): string;
var
  V: variant;
  val: string;
  i, j, cntRecs, cntFields: Integer;
begin
  Result := '';
{$IFDEF FULLDEBUG}
  WriteToLog(Format('GetSQLValue : %s', [SQLText]), 'debug.log');
{$ENDIF}
  V := ExecSql(SQLText);
  cntRecs := VarArrayHighBound(V[1], 1);

  for i := 0 to cntRecs do
  begin
    cntFields := VarArrayHighBound(V[1][i], 1);
    for j := 0 to cntFields do
    begin
      val := VarToStr(V[1][i][j]);
      Result := Result + val;
      if j <> cntFields then
        Result := Result + Delim4Fields;
    end;
    if i <> cntRecs then
      Result := Result + Delim4Recs;
  end;
end;

{ procedure TfrmRepl.KeepClientsBusy;
  begin
  ExecSql('insert into sync(EXPR, TableName, CallType) Values(''Select Count(1) From System.iota Where 1=1 / Select 1 From System.iota'', ''NONE'', ''KeepBusy'')', '/')
  end; }

function TfrmRepl.SetNewConnection: string;
begin
  Result := '';
  oDlg.InitialDir := ExtractFileDir(Application.ExeName);
{$IFDEF SYRCLIENT}
  if MessageBox(Handle, PChar('Browse for folder with free tables?'), 'DB', MB_YESNO) = ID_YES then
  begin
    if not SelectDirectory('Select folders with free tables', '', Result) then
    begin
      Exit;
    end;
  end
  else
{$ENDIF}
  begin
    if not oDlg.Execute then
    begin
      Exit;
    end;
    Result := oDlg.FileName;
  end;
  edCon.Text := Result;
end;

procedure TfrmRepl.btnSetConClick(Sender: TObject);
begin
  // con.IsConnected:=False;
  SetNewConnection;
  // con.IsConnected:=True;
end;

procedure TfrmRepl.Button1Click(Sender: TObject);
begin
  if serv.Active then
  begin
    Button1.Caption := 'Start';
    serv.Active := False;
    servTimer.Enabled := False;
  end
  else
  begin
    Button1.Caption := 'Stop';
    ListenServer;
{$IFNDEF SYRCLIENT}
    StartServTimer;
{$ENDIF}
  end;
end;

{$IFDEF SYRCLIENT}

procedure TfrmRepl.SendTimeDataReport;
var
  ASubject: string;
begin
  qryCheckTime.Close;
  qryCheckTime.Open;
  if qryCheckTime.IsEmpty then
    dmSendEmail.SendMessage('No Time Sync', 'No Record for Today.')
  else
  begin
    if qryCheckTime.FieldByName('Diff').AsInteger > 90 then
      ASubject := 'DELAYED Update'
    else
      ASubject := 'Update';
    dmSendEmail.SendMessage(ASubject, Format('Last update received %s, Count %s, Minutes elapsed %s',
      [qryCheckTime.FieldByName('Stamp').AsString, qryCheckTime.FieldByName('Cnt').AsString,
      qryCheckTime.FieldByName('Diff').AsString]))
  end;
end;
{$ENDIF}

procedure TfrmRepl.ListenServer;
begin
  serv.Active := True;
end;

procedure TfrmRepl.servDisconnect(AContext: TIdContext);
begin
  // WriteToLog('servDisconnect, Disconnected', 'err.log');
end;

procedure TfrmRepl.servException(AContext: TIdContext; AException: Exception);
begin
  if Pos('Connection Closed Gracefully', AException.Message) = 0 then
    WriteToLog('servException, Error: ' + AException.Message, 'err.log');

  if Pos('Operation aborted', AException.Message) > 0 then
  begin
    servTimer.Enabled := False;
    FStartListenServer := True;
    FInServExecute := False;
    serv.Active := False;
  end
end;

procedure TfrmRepl.servExecute(AThread: TIdContext);
var
  S, ip, cmd, sCheck, sComp: string;
  cmdPos: Integer;
begin
  if FStartListenServer then
    Exit;
  FInServExecute := True;
  try
    try
      S := AThread.Connection.IOHandler.ReadLn(CUSTOM_TERMINATOR);
    except
      on e: Exception do
      begin
        if Pos('10107', e.Message) > 0 then
        begin
          servTimer.Enabled := False;
          FStartListenServer := True;
          // AThread.Connection.IOHandler.Close;
          serv.Active := False;
          Exit; // might replace with sysutils.abort which might require FInServExecute := False
        end
        else
          raise;
      end;
    end;
    try
{$IFNDEF SYRCLIENT}
      // SERVER CODE
      // SendDebugEx('servExecute', mtInformation);
      cmd := Copy(S, 1, Pos(' ', S) - 1);
      ip := GetClientCompName(AThread);
{$IFNDEF LOCALTEST}
      sComp := trim(StringReplace(S, cmd, '', [])) + '[' + ip + ']';
{$ELSE}
      sComp := trim(StringReplace(S, cmd, '', []));
{$ENDIF}
      (*
        sComp := UpperCase(GetClientCompName(AThread));
        if trim(s) = 'add' then
        AddComp(sComp);
        if trim(s) = 'remove' then
        RemoveComp(sComp);
        if trim(s) = 'hello' then
        ExecSql(Format('update sync_comp set lastsync=now() where comp=''%s''',
        [sComp]));
      *)
      if trim(cmd) = 'add' then
      begin
        AddComp(sComp);
{$IFDEF NewWorkerThreads}
        startWorker(sComp, False);
{$ENDIF}
      end;
      if trim(cmd) = 'remove' then
      begin
        RemoveComp(sComp);
{$IFDEF NewWorkerThreads}
        removeWorker(sComp, True);
{$ENDIF}
      end;
      if trim(cmd) = 'hello' then
      begin
        ExecSql(Format('update sync_comp set lastsync=now() where comp=''%s''', [sComp]));
      end;
      Exit;
{$ENDIF}
      if trim(S) = 'hello' then
      begin
        // SetGlobalEnvironment('CDSYNC_LAST_HELLO', DateTimeToStr(Now()), False);
        AThread.Connection.IOHandler.Write('recv' + CUSTOM_TERMINATOR);
        Exit;
      end;

      try
        if Pos('select', lowercase(trim(S))) = 1 then
        begin
          cmdPos := Pos('/ insert', lowercase(S));
          sCheck := Copy(S, 1, cmdPos - 1);
          //WriteToLog('Check for duplicates: ' + sCheck, 'err.log');
          if GetSQLValue(sCheck) <> '0' then
          begin
            WriteToLog('Duplicate Record! ' + sCheck, 'err.log');
            AThread.Connection.IOHandler.Write('done' + CUSTOM_TERMINATOR);
            Exit;
          end;
          S := Copy(S, cmdPos + 1, Length(S));
        end;

        if ExecSql(S) <> 1 then
        begin
          AThread.Connection.IOHandler.Write('error' + CUSTOM_TERMINATOR);
          Exit; // DP - bad idea, in case of error, what?!
          // raise EAbort.Create('Error executing '+s);
        end;
        AThread.Connection.IOHandler.Write('done' + CUSTOM_TERMINATOR);
        // SetGlobalEnvironment('CDSYNC_LAST_HELLO', DateTimeToStr(Now()), False);
        // SetGlobalEnvironment('CDSYNC_LAST_UPDATE', DateTimeToStr(Now()), False);
      finally
      end;
    except
      on e: Exception do
      begin
        AThread.Connection.IOHandler.Write(Format('error : %s on %s' + CUSTOM_TERMINATOR, [e.Message, S]));
        WriteToLog(Format('error (5): %s on %s', [e.Message, S]), 'err.log');
      end;
    end;
  finally
    FInServExecute := False;
  end;
end;

procedure TfrmRepl.servListenException(AThread: TIdListenerThread; AException: Exception);
begin
  WriteToLog('servListenException, Error: ' + AException.Message, 'err.log');

  if Pos('The requested IPVersion / Address family is not supported', AException.Message) > 0 then
  begin
    WriteToLog('servListenException, Try Restart Start for IPVersion / Address', 'err.log');
    servTimer.Enabled := False;
    FStartListenServer := True;
    FInServExecute := False;
    serv.Active := False;
    servTimer.Enabled := True;
    WriteToLog('servListenException, Try Restart End for IPVersion / Address', 'err.log');
  end;
end;

procedure TfrmRepl.AddComp(sComp: string);
var
  sCnt: string;
begin
  sCnt := GetSQLValue(Format('select count(1) from sync_comp where comp=''%s''', [sComp]));
  if (sCnt = '0') or (sCnt = '') then
    ExecSql(Format('insert into sync_comp(comp) values(''%s'')', [sComp]));
end;

procedure TfrmRepl.RemoveComp(sComp: string);
begin
  ExecSql(Format('delete from sync_comp where comp=''%s''', [sComp]));
end;

procedure TfrmRepl.btnRegClick(Sender: TObject);
{$IFDEF SYRCLIENT}
var
  sServ, SClient: string;
{$ENDIF}
begin
{$IFDEF SYRCLIENT}
  if trim(edServer.Text) = '' then
  begin
    ShowMessage('Please enter server name!');
    Exit;
  end;

  if trim(edClientName.Text) = '' then
  begin
    ShowMessage('Please enter client name!');
    Exit;
  end;

  SClient := trim(edClientName.Text);
  sServ := GetRegStringValue(CustomReg, 'Server', HKEY_LOCAL_MACHINE);

  if (sServ <> '') and (sServ <> edServer.Text) then
  begin
    // don'ty care for error if can't reach old server
    try
      client.Host := trim(sServ);
      client.Connect;
      // client.IOHandler.WriteLn('remove');
      client.IOHandler.Write('remove ' + SClient + CUSTOM_TERMINATOR);
      client.IOHandler.InputBuffer.Clear;
      client.Disconnect;
    except
    end;
  end;
  CreateRegKey(CustomReg, 'Server', edServer.Text, HKEY_LOCAL_MACHINE);
  CreateRegKey(CustomReg, 'Client', edClientName.Text, HKEY_LOCAL_MACHINE);
  if (edtPort.Text <> '') and (edtPort.Text <> '8760') then
    CreateRegKey(CustomReg, 'Port', edtPort.Text, HKEY_LOCAL_MACHINE);
  client.Host := trim(edServer.Text);
  client.Port := ConnectPort + 1;
  client.Connect;
  // client.IOHandler.WriteLn('add');
  client.IOHandler.Write('add ' + SClient + CUSTOM_TERMINATOR);
  client.IOHandler.InputBuffer.Clear;
  client.Disconnect;

  ShowMessage('Successfully Registered With Server.');
{$ELSE}
  if (edtPort.Text <> '') and (edtPort.Text <> '8760') then
  begin
    CreateRegKey(CustomReg, 'Port', edtPort.Text, HKEY_LOCAL_MACHINE);
    ShowMessage('Port Set.');
  end;
{$ENDIF}
end;

function IPAddrToName(IPAddr: string): string;
var
  SockAddrIn: TSockAddrIn;
  HostEnt: PHostEnt;
  WSAData: TWSAData;
  ind: Integer;
begin
  WSAStartup($101, WSAData);
  SockAddrIn.sin_addr.s_addr := inet_addr(PAnsiChar(AnsiString(IPAddr)));
  HostEnt := gethostbyaddr(@SockAddrIn.sin_addr.s_addr, 4, AF_INET);
  if HostEnt <> nil then
    Result := string(StrPas(HostEnt^.h_name))
  else
    Result := '';

  ind := Pos('.', Result);
  if ind > 0 then
    Result := Copy(Result, 1, ind);
end;

function TfrmRepl.GetClientCompName(AThread: TIdContext): string;
{$IFNDEF LOCALTEST}
var
  ip: string;
{$ENDIF}
begin
{$IFNDEF LOCALTEST}
  ip := AThread.Connection.Socket.Binding.PeerIP;
  // Result := IPAddrToName(ip);
  Result := ip;
{$ELSE}
  Result := Format('%d[localhost]', [AThread.Connection.Socket.Binding.Port]);
{$ENDIF}
end;

procedure TfrmRepl.ReadConnectionSettings;
var
  ConStr, ConChkStr: string;
begin
  con.IsConnected := False;
{$IFDEF SYRCLIENT}
  ConStr := GetRegStringValue(CustomReg, 'DBLocalPath', HKEY_LOCAL_MACHINE);
  if ConStr = '' then
    ConStr := 'C:\PosLocal\';
{$ELSE}
  ConStr := GetRegStringValue(CustomReg, 'DBPath', HKEY_LOCAL_MACHINE);

  chkAllowLocal.Checked := StrToBoolDef(GetRegStringValue(CustomReg, 'UseALS', HKEY_LOCAL_MACHINE), False);
  if not chkAllowLocal.Checked then
    con.AdsServerTypes := [stADS_REMOTE]
  else
    con.AdsServerTypes := [stADS_REMOTE, stADS_LOCAL];

  ConChkStr := ReplaceStr(ConStr, ':6262', '');
  if IsDesktopMode and ((ConStr = '') or not(FileExists(ConChkStr) or SysUtils.DirectoryExists(ConChkStr))) then
    ConStr := SetNewConnection;
{$ENDIF}
  edCon.Text := ConStr;
{$IFNDEF LOCALTEST}
  con.ConnectPath := ConStr;
  con.Username := 'Dev';
  con.Password := UserPass;
{$ELSE}
  con.ConnectPath := 'X:\pdelphi\CdSyncData\Server2\TisWin3DD.add';
  con.Username := '1';
  con.Password := '1';
{$ENDIF}
  con.IsConnected := True;
end;

procedure TfrmRepl.servTimerTimer(Sender: TObject);
{$IFDEF SYRCLIENT}
var
  sServ: string;
{$ENDIF}
begin
{$IFDEF SYRCLIENT}
  if FInServExecute then
    Exit;
{$ENDIF}
  servTimer.Enabled := False;
  try
{$IFNDEF SYRCLIENT}
    // SendDebugEx('servTimerTimer - Server', mtInformation);
{$IFNDEF NewWorkerThreads}
    SendUpdatesNew;
{$ENDIF}
{$ELSE}
    // SendDebugEx('servTimerTimer - Client', mtInformation);
    try
      if FStartListenServer then
      begin
        WriteToLog('servTimerTimer found FStartListenServer is set', 'err.log');
        FStartListenServer := False;
        serv.Active := False;
        Sleep(5000);
        WriteToLog('servTimerTimer calling ListenServer Start', 'err.log');
        ListenServer;
        WriteToLog('servTimerTimer calling ListenServer End', 'err.log');
        Exit;
      end;

      if not ClientBusy.Enabled then
        ClientBusy.Enabled := True;

      if client.Connected then
      begin
        client.IOHandler.InputBuffer.Clear;
        client.IOHandler.CloseGracefully;
        client.Disconnect;
      end;
{$IFNDEF LOCALTEST}
      sServ := trim(GetRegStringValue(CustomReg, 'Server', HKEY_LOCAL_MACHINE));
{$ELSE}
      sServ := 'localhost';
{$ENDIF}
      if sServ = '' then
        Exit;
      client.Host := sServ;
      client.Connect;
      client.IOHandler.Write('hello' + CUSTOM_TERMINATOR);
      client.IOHandler.InputBuffer.Clear;
      client.IOHandler.CloseGracefully;
      client.Disconnect;
    except
      on e: Exception do
        WriteToLog(Format('Sync Msg to host %s Error:', [sServ]) + e.Message, 'err.log');
    end;
{$ENDIF}
  finally
    servTimer.Enabled := True;
  end;
end;

function TfrmRepl.existsWorker(aclientId: string): Boolean;
var
  n: Integer;
  aWorkerTask: IOmniWorker;
begin
  Result := False;
  for n := 0 to (FSendWorkerTaskList.Count - 1) do
  begin
    aWorkerTask := FSendWorkerTaskList[n];
    if TSendWorker(aWorkerTask.Implementor).isForClient(aclientId) then
    begin
      Result := True;
      break;
    end;
  end;
end;

procedure TfrmRepl.removeWorker(aclientId: string; isRemoveCommand: Boolean = False);
var
  n: Integer;
  aWorkerTask: IOmniWorker;
begin
  for n := 0 to (FSendWorkerTaskList.Count - 1) do
  begin
    aWorkerTask := FSendWorkerTaskList[n];
    if TSendWorker(aWorkerTask.Implementor).isForClient(aclientId) then
    begin
      if isRemoveCommand then
        WriteToLog(Format('Worker %s being removed on REMOVE command', [aclientId]), 'appp.log');
      if (aWorkerTask.task <> nil) and (not aWorkerTask.task.terminated) then
      begin
        if isRemoveCommand then
          aWorkerTask.task.SetExitStatus(WORKER_EXIT_REMOVE_COMMAND, aclientId);
        aWorkerTask.task.Terminate;
      end;
      FSendWorkerTaskList[n] := nil;
      FSendWorkerTaskList.Delete(n);
      break;
    end;
  end;
end;

procedure TfrmRepl.startWorker(aclient: string; clientIsGettingErrors: Boolean
  // in such case, the worker tries for very few updates periodically
  );
var
  FWorkerTask: IOmniTaskControl;
  FWorker: IOmniWorker;
  FWakeTime: Integer;
begin
  FWorker := TSendWorker.Create(aclient, clientIsGettingErrors);

  FWakeTime := WORKER_WAKEUP_DELAY_MSECS;

  FWorkerTask := CreateTask(FWorker, aclient).SetTimer(1, FWakeTime, MSG_WAKESENDWORKER).monitorWith(oem2).schedule;
  // Run;
  // previous task has to be freed
  removeWorker(aclient);
  FSendWorkerTaskList.Add(FWorker);
end;

procedure TfrmRepl.StartServTimer;
const
  // Time in minutes to recheck an "offline" client to see if
  // it needs to come back into the processing loop.
  OfflineClientRecheckInterval: Integer = 10;
begin
  servTimer.Enabled := True;
{$IFNDEF SYRCLIENT}
{$IFDEF NewWorkerThreads}
  startClientWorkers;
{$ENDIF}
  cleanupDownlistTimer.Interval := OfflineClientRecheckInterval * 60 * 1000;
  cleanupDownlistTimer.Enabled := True;
{$ENDIF}
end;

procedure TfrmRepl.ExecSqlSync(var msg: TMessage);
begin
  sSyncExecSqlRet := ExecSql(sSyncSql);
end;

{$IFNDEF SYRCLIENT}

procedure TfrmRepl.UpdateSyncTimeData;
begin
  ExecSql('Try Merge SyncTime ON (Created=CurDate()) When Matched Then Update Set Cnt=Cnt+1 ' +
    'When Not Matched Then Insert (ID) Values(NewIdString("B")); Catch All End;');
end;

// Add a client to the Down List (Sync_down) table
procedure TfrmRepl.AddCompDown(sComp: string);
var
  sCnt: string;
begin
  try
  sCnt := GetSQLValue(Format('Select Count(1) From Sync_Down Where Comp=''%s''', [sComp]));
  if (sCnt = '0') or (sCnt = '') then
    ExecSql(Format('Insert Into Sync_Down(comp) Values(''%s'')', [sComp]));
  except

  end;
end;
{$ENDIF}
{$IFDEF UNUSED_TRIED}

// Used by PrepareDownList that is not used now
function TfrmRepl.isClientOnline(SClient: string): Boolean;
var
  ip, Port, res: string;
begin
  Result := False;
{$IFNDEF LOCALTEST}
  ip := Copy(SClient, Pos('[', SClient) + 1, Length(SClient));
  ip := Copy(ip, 1, Pos(']', ip) - 1);
{$ELSE}
  ip := 'localhost';
  Port := Copy(SClient, 1, Pos('[', SClient) - 1);
{$ENDIF}
  // If the client is busy then it's online
  if IsClientBusy(
{$IFNDEF LOCALTEST}
    ip
{$ELSE}
    SClient
{$ENDIF}
    ) then
  begin
    Result := True;
    Exit;
  end;

  if client.Connected then
  begin
    client.IOHandler.InputBuffer.Clear;
    client.IOHandler.CloseGracefully;
    client.Disconnect;
  end;
  client.Host := ip;
{$IFNDEF LOCALTEST}
  client.Port := ConnectPort;
{$ELSE}
  client.Port := StrToInt(Port);
{$ENDIF}
  try
    // We just do a connection test
    client.Connect;
    { client.IOHandler.WriteLn('hello');
      res := client.IOHandler.ReadLn;
      result := res = 'recv'; }
    client.IOHandler.InputBuffer.Clear;
    client.IOHandler.CloseGracefully;
    client.Disconnect;
    Result := True; // could connect
  except
  end;
end;

// Tried this to prepare the down list table at the beginning
// of the send updates loop. But this is too slow. Not used any more.
// New strategy is to add to down list table on any error and clear
// it on a timer of about 10 minutes.
procedure TfrmRepl.PrepareDownList;
var
  sComp: string;
  slist: TStringList;
  i: Integer;
begin
  slist := TStringList.Create;
  try
    if tbSyncComp.Active then
      tbSyncComp.Close;
    tbSyncComp.Open;

    tbSyncComp.First;
    while not tbSyncComp.EOF do
    begin
      sComp := tbSyncComp.FieldByName('Comp').AsString;
      slist.Add(sComp);
      tbSyncComp.Next;
    end;
    ExecSql('Try Delete From Sync_Down; Catch All End;');
    for i := 0 to slist.Count - 1 do
    begin
      if not isClientOnline(slist[i]) then
      begin
        AddCompDown(slist[i]);
      end;
    end;
  finally
    slist.Free;
  end;
end;
{$ENDIF}

function TfrmRepl.UserPass: string;
var
  i: Integer;
begin
  Result := 'l' + chr(101) + chr(122) + 'a' + chr(77);
  for i := 1 to 5 do
    Result := Result + IntToStr(i);
  Result := Result + '$$';
  { for i := 1 to 3 do
    Result := Result + IntToStr(i);
    Result := Result + '$5'; }
end;

// New threads logic
{$IFNDEF SYRCLIENT}

procedure TfrmRepl.startClientWorkers;
var
  clientList: TStringList;
  i: Integer;
begin
  GlobalOmniThreadPool.monitorWith(oem2);
  // by default, the following is equal to number of Cores only
  GlobalOmniThreadPool.MaxExecuting := WORKER_MAX_THREADS;
  // no limit is placed on queue

  // The new code will rather start the workers
  clientList := TStringList.Create;
  try
    getClientList(clientList);
    for i := 0 to clientList.Count - 1 do
      startWorker(clientList[i], False);

  finally
    clientList.Free;
  end;
end;
{$ENDIF}

procedure TfrmRepl.SendUpdatesNew;
var
  ip, sId, sComp, sExpr, highestDone: string;
  AIndex, ADone: Integer;
  ticks: cardinal;
  sentCount: Integer;
  numClients, numberOfUpdatesToPerform: Integer;
  sCnt: string;
const
  // In each Send Updates loop, process so many updates for
  // each "Online" client
  NumUpdatesPerClient: Integer = 10;

  numUpdatesPerClientToPerform: Integer = 10;
  // The following is needed because TOP can not be parameterized
  sqlForProcess =
    'Select Top %d y.ID, y.Comp, x.Expr From Sync x Left Join Sync_Msg y ON y.ID=x.ID Where y.ID>0 And x.ID>0 And y.Comp Not IN (Select * From Sync_Down) Order By x.ID;';
begin
  sCnt := GetSQLValue(Format('Select Count(1) From Sync_Comp', [sComp]));
  // So only one count query now
  // sDown := GetSQLValue(Format('select count(1) from sync_down', [sComp]));
  try
    // active clients to consider
    numClients := StrToInt(sCnt); // avoid - strtoint(sDown);
    numberOfUpdatesToPerform := NumUpdatesPerClient * numClients;
  except
    numberOfUpdatesToPerform := 150; // default
  end;
  qryProcess.Sql.Text := Format(sqlForProcess, [numberOfUpdatesToPerform]);

  sentCount := 0;
  FInSendUpdatesNew := True;
  try
    // FSkipList.Sort; //no longer needed as sorted list
    try
      WriteToLog('Before SendUpdatesNew - 5', 'trc.log');

      if not qryProcess.Prepared then
        qryProcess.Prepare;

      qryProcess.Close;
      qryProcess.Open;

      qryProcess.First;
      ticks := GetTickCount;
      while not qryProcess.EOF do
      begin
{$IFNDEF SYRCLIENT}
        CheckBusyStatus;
{$ENDIF}
        sId := qryProcess.FieldByName('ID').AsString;
        sComp := qryProcess.FieldByName('Comp').AsString;
        sExpr := qryProcess.FieldByName('Expr').AsString;

        ip := Copy(sComp, Pos('[', sComp) + 1, Length(sComp));
        { corrected here - the first sComp was sId }
        ip := Copy(ip, 1, Pos(']', ip) - 1);

        // If client is in skiplist, skip it.
        // Now, it is in skiplist when busy or
        // when error encountered in this loop.
        if FSkipList.Find(sComp, AIndex) then
        begin
          // SendDebugEx('Client is busy! Update postponed.', mtError);
          qryProcess.Next;
          Continue;
        end;

        // FIX: for sequence problem. If client is busy, skip it for the rest of the loop
        // to avoid out of sequence problem
        if IsClientBusy(
{$IFNDEF LOCALTEST}
          ip
{$ELSE}
          sComp
{$ENDIF}
          ) then
        begin
          FSkipList.Add(sComp); // fixL for seq
          qryProcess.Next;
          Continue;
        end;

        {
          // Set busy flag
          SetBusyFlag(ip, True);
        }

        // Better log only when confirmed that it is done.
        // So put in the later step below.
        // WriteToLog(Format('Send to %s Sql : ', [sComp]) + sExpr, 'appp.log');
        ADone := Send2Client(sComp, sId, sExpr, ConnectPort);

        if ADone <> 1 then
        begin
          highestDone := sId;
          sentCount := sentCount + 1;
          WriteToLog(Format('Send to %s Sql : ', [sComp]) + sExpr, 'appp.log');
        end;

        {
          if not Send2Client(sComp, sId, sExpr) then
          // Client did not respond, remove for current loop
          begin
          WriteToLog('Adding ' + sComp + ' to SkipList', 'AppSkip.log');
          FSkipList.Add(sComp);
          end;


          // Clear busy flag
          SetBusyFlag(ip, False);
        }

        if Application.terminated then
          Exit;

        if ADone <> 1 then
          qryProcess.Next // ADone=1 indicates MAX_TASKS, retry same record rather than moving to next
        else
        begin
          Application.ProcessMessages; // w/o processmEssages the threads never release.
        end;
        // 09/16/13, might have to add a maximum time as this will run for always if threads don't clear

        // Application.ProcessMessages;
        // Sleep(0);
      end;
      ticks := GetTickCount - ticks;
      if ticks > 0 then
      begin
        WriteToLog(Format(' %d msecs, sent recs: %d, highest done: %s', [ticks, sentCount, highestDone]), 'appp.log');
      end;

{$IFNDEF SYRCLIENT}
      CheckBusyStatus;
      FSkipList.Clear;
{$ENDIF}
    except
      on e: Exception do
        WriteToLog('Exception (4) : ' + e.Message + #13#10 + sExpr, 'err.log');
    end;
  finally
    FInSendUpdatesNew := False;
    con.Disconnect;
  end;
  WriteToLog('After SendUpdatesNew - 5', 'trc.log');
end;

procedure _Send2Client(const task: IOmniTask);
var
  tcpClnt: TIdTCPClient;
  res, ip
{$IFDEF LOCALTEST}
    , Port
{$ENDIF}
    : string;
  SClient, sId, sUpd: string;
  param: TSend2ClientTaskParam;
begin

{$IFDEF NewOmniLib}
  // change due to new OmniThread library
  param := task.param['Send2ClientTaskParam'].ToRecord<TSend2ClientTaskParam>;
{$ELSE}
  param := task.param['Send2ClientTaskParam'].AsRecord<TSend2ClientTaskParam>;
{$ENDIF}
  SClient := param.tpClient;
  sId := param.tpId;
{$IFNDEF LOCALTEST}
  sUpd := param.tpUpd;
  ip := Copy(SClient, Pos('[', SClient) + 1, Length(SClient));
  ip := Copy(ip, 1, Pos(']', ip) - 1);
  SetBusyFlag(ip, True);
{$ELSE}
  // prepend with id
  // IMPORTANT: This is the trick for our local testing. We
  // prepend the tpId to the string itself so that the client
  // can check out of sequence.
  sUpd := sId + ' ' + param.tpUpd;
  ip := 'localhost';
  Port := Copy(SClient, 1, Pos('[', SClient) - 1);
  SetBusyFlag(SClient, True);
{$ENDIF}
  tcpClnt := TIdTCPClient.Create;
  try
    try
{$IFNDEF LOCALTEST}
      tcpClnt.Port := param.tpPort;
{$ELSE}
      tcpClnt.Port := StrToInt(Port);
{$ENDIF}
      tcpClnt.ReadTimeout := 0;
      tcpClnt.Host := ip;
      tcpClnt.Connect;
      WriteToLog(sUpd, 'err.log');
      tcpClnt.IOHandler.Write(sUpd + CUSTOM_TERMINATOR);
      res := tcpClnt.IOHandler.ReadLn(CUSTOM_TERMINATOR);
      if trim(res) = 'done' then
        task.Comm.Send(MSG_SEND2CLIENT_DONE, [sId, SClient, ip])
{$IFDEF LOCALTEST}
      else if trim(res) = 'halted' then
      begin
        WriteToLog(sId + ' received halt-out-of-sequence', 'appp.log');
        raise Exception.Create('Halt out of sequence');
      end
{$ENDIF}
        ;
      tcpClnt.IOHandler.InputBuffer.Clear;
      tcpClnt.IOHandler.CloseGracefully;
      tcpClnt.Disconnect;
    except
      on e: Exception do
      begin
        if tcpClnt.Connected then
        begin
          tcpClnt.IOHandler.InputBuffer.Clear;
          tcpClnt.IOHandler.CloseGracefully;
        end;
        tcpClnt.Disconnect;
        task.Comm.Send(MSG_SEND2CLIENT_EXCEPT, [SClient, e.Message]);
      end;
    end;
  finally
{$IFNDEF LOCALTEST}
    SetBusyFlag(ip, False);
{$ELSE}
    SetBusyFlag(SClient, False);
{$ENDIF}
    tcpClnt.Free;
  end;
end;

function TfrmRepl.Send2Client(SClient, sId, sUpd: string; iPort: Integer): Integer;
var
  Send2ClientTaskParam, Send2ClientTaskParam0: TSend2ClientTaskParam;
  n: Integer;
  FSend2ClientTask: IOmniTaskControl;
begin
  Result := 0;
  Inc(FLastTaskID);

  Send2ClientTaskParam.tpTaskID := FLastTaskID;
  Send2ClientTaskParam.tpClient := SClient;
  Send2ClientTaskParam.tpId := sId;
  Send2ClientTaskParam.tpUpd := sUpd;
  Send2ClientTaskParam.tpPort := ConnectPort;

  if FSend2ClientTaskList.Count = MAX_TASKS then
  begin
    Result := 1;
    Exit;
  end;

  // Added 07/25/16 - to prevent more than one thread running for same Client. Otherwise we are not going to have lates values
  // We should add multiple tbSyncMsg to send to multiple Clients at once for cases where one machine has lots of backlog
  for n := 0 to (FSend2ClientTaskList.Count - 1) do
  begin
{$IFDEF NewOmniLib}
    // change due to new omnithread lib
    Send2ClientTaskParam0 := FSend2ClientTaskList[n].param['Send2ClientTaskParam'].ToRecord<TSend2ClientTaskParam>;
{$ELSE}
    Send2ClientTaskParam0 := FSend2ClientTaskList[n].param['Send2ClientTaskParam'].AsRecord<TSend2ClientTaskParam>;
{$ENDIF}
    if SameText(Send2ClientTaskParam0.tpClient, SClient) then
    begin
      Result := 1;
      Exit;
    end;
  end;

  FSend2ClientTask := CreateTask(_Send2Client, '_Send2Client').SetQueueSize(10000).monitorWith(oem1)
    .SetParameter('Send2ClientTaskParam', TOmniValue.FromRecord(Send2ClientTaskParam)).Run;

  WriteToLog(Format('FSend2ClientTaskList.Add: %d ', [LongInt(FSend2ClientTask)]), 'FSend2ClientTaskList.log');
  FSend2ClientTaskList.Add(FSend2ClientTask);
end;

procedure TfrmRepl.oem1TaskMessage(const task: IOmniTaskControl; const msg: TOmniMessage);
var
  SClient, sId, sEMsg, ip: string;
  AErrorID: Integer;
begin
  case msg.MsgID of
    MSG_SEND2CLIENT_DONE:
      begin
        sId := msg.MsgData[0];
        SClient := msg.MsgData[1];
        ip := msg.MsgData[2];
        ExecSql(Format('delete from sync_msg where id=%s and comp=''%s''', [sId, SClient]));
        WriteToLog(Format('Done id=%s comp=%s', [sId, SClient]), 'Done.log');

        { SetBusyFlag(ip, False); }
      end;
    MSG_SEND2CLIENT_EXCEPT:
      begin
        SClient := msg.MsgData[0];
        sEMsg := msg.MsgData[1];
        WriteToLog(Format('Exception (3) Send2Client %s: ', [SClient]) + sEMsg, 'err.log');
        AErrorID := 0;
        if Pos('Socket Error # ', sEMsg) > 0 then
        begin
          try
            AErrorID := StrToInt(Copy(sEMsg, 16, 10));
          except
          end;
          if Assigned(CDReplicationService) then
            CDReplicationService.LogMessage(Format('Exception (3) Send2Client %s: %s', [SClient, sEMsg]), 1, 0,
              AErrorID);
        end;

        // Client did not respond, remove for current loop
        WriteToLog('Adding ' + SClient + ' to SkipList', 'AppSkip.log');
        FSkipList.Add(SClient);

        // Also add to downlist
{$IFNDEF SYRCLIENT}
        AddCompDown(SClient);
{$ENDIF}
        WriteToLog('Adding ' + SClient + ' to down list', 'appp.log');
      end;
  end;
end;

procedure TfrmRepl.oem1TaskTerminated(const task: IOmniTaskControl);
var
  n: Integer;
  Send2ClientTaskParam0: TSend2ClientTaskParam;
  Send2ClientTaskParam1: TSend2ClientTaskParam;
begin

{$IFDEF NewOmniLib}
  // change due to new OmniThread library
  Send2ClientTaskParam0 := task.param['Send2ClientTaskParam'].ToRecord<TSend2ClientTaskParam>;
{$ELSE}
  Send2ClientTaskParam0 := task.param['Send2ClientTaskParam'].AsRecord<TSend2ClientTaskParam>;
{$ENDIF}
  for n := 0 to (FSend2ClientTaskList.Count - 1) do
  begin
    // change due to new OmniThread library
{$IFDEF NewOmniLib}
    Send2ClientTaskParam1 := FSend2ClientTaskList[n].param['Send2ClientTaskParam'].ToRecord<TSend2ClientTaskParam>;
{$ELSE}
    Send2ClientTaskParam1 := FSend2ClientTaskList[n].param['Send2ClientTaskParam'].AsRecord<TSend2ClientTaskParam>;
{$ENDIF}
    if Send2ClientTaskParam0.tpTaskID = Send2ClientTaskParam1.tpTaskID then
    begin
      WriteToLog(Format('FSend2ClientTaskList.Delete: %d ', [LongInt(FSend2ClientTaskList[n])]),
        'FSend2ClientTaskList.log');
      FSend2ClientTaskList[n] := nil;
      FSend2ClientTaskList.Delete(n);

      break;
    end;
  end;

end;

{ TStringListEx }

function TStringListEx.Find(const S: string; var Index: Integer): Boolean;
var
  L, H, i, c: Integer;
begin
  Result := False;
  L := 0;
  H := Count - 1;
  while L <= H do
  begin
    i := (L + H) shr 1;
    c := CompareStr(Get(i), S);
    if c < 0 then
      L := i + 1
    else
    begin
      H := i - 1;
      if c = 0 then
      begin
        Result := True;
        if Duplicates <> dupAccept then
          L := i;
      end;
    end;
  end;
  Index := L;
end;

// new stuff

Constructor TSendWorker.Create(aclientId: string; clientGotError: Boolean = False);
begin
  inherited Create;
  fClientId := aclientId;
  fUpdatesList := TList<TSendWorkerInfo>.Create;
  fAskedForUpdates := False;
  fBusyInUpdate := False;
  fClientInError := clientGotError;
end;

Destructor TSendWorker.Destroy;
begin
  fUpdatesList.Free;
  inherited;
end;

function TSendWorker.Initialize: Boolean;
begin
  Result := True;
end;

function TSendWorker.isForClient(aclientId: string): Boolean;
begin
  Result := SameText(aclientId, fClientId);
end;

procedure TSendWorker.sendNextUpdate;
begin
  if (fUpdatesList.Count = 0) or (fAskedForUpdates) or (fBusyInUpdate) then
    Exit;
  WriteToLog(Format('Worker %s sending update %s: %s', [fClientId, fUpdatesList[0].tpId, fUpdatesList[0].tpUpd]),
    'appp.log');

  Send2Client(fUpdatesList[0].tpId, fUpdatesList[0].tpUpd, fUpdatesList[0].tpPort);
end;

procedure TSendWorker.verifySidAndDeleteUpdate(sId: string);
begin
  if fUpdatesList[0].tpId = sId then
  begin
    fUpdatesList.Delete(0);
    if fUpdatesList.Count = 0 then
    begin
      task.SetExitStatus(WORKER_EXIT_LIST_DONE, fClientId);
      task.Terminate;
    end;
  end
  else
    WriteToLog(Format('Worker %s got internal error delete on Done request for sid: %s', [fClientId, sId]), 'appp.log');
end;

procedure TSendWorker.Send2Client(tpId, tpUpd: string; tpPort: Integer);
var
  tcpClnt: TIdTCPClient;
  res, ip
{$IFDEF LOCALTEST}
    , Port
{$ENDIF}
    : string;
  sId, sUpd: string;
begin
  sId := tpId;
{$IFNDEF LOCALTEST}
  sUpd := tpUpd;
  ip := Copy(fClientId, Pos('[', fClientId) + 1, Length(fClientId));
  ip := Copy(ip, 1, Pos(']', ip) - 1);
{$ELSE}
  // prepend with id
  // IMPORTANT: This is the trick for our local testing. We
  // prepend the tpId to the string itself so that the client
  // can check out of sequence.
  sUpd := sId + ' ' + tpUpd;
  ip := 'localhost';
  Port := Copy(fClientId, 1, Pos('[', fClientId) - 1);
{$ENDIF}
  // but this flag will be reset only in TaskMessage receiver
  fBusyInUpdate := True;

  // To test the workers without tcpip sending, uncomment the following lines
  { sleep(50);
    task.Comm.Send(MSG_SEND2CLIENTNEW_DONE, [sId, fClientId, ip]);
    verifySidAndDeleteUpdate(sId);
    exit; }

  tcpClnt := TIdTCPClient.Create;
  try
    try
{$IFNDEF LOCALTEST}
      tcpClnt.Port := tpPort;
{$ELSE}
      tcpClnt.Port := StrToInt(Port);
{$ENDIF}
      tcpClnt.ReadTimeout := 0;
      tcpClnt.Host := ip;
      tcpClnt.Connect;
      //This should be changed to NOT REMOVING CRLF and instead replace the ReadLn stuff that causes the problems
      {sUpd := ReplaceStr(sUpd, #13#10, ' ');
      sUpd := ReplaceStr(sUpd, #10#13, ' ');
      sUpd := ReplaceStr(sUpd, #10, ' ');
      sUpd := ReplaceStr(sUpd, #13, ' ');}
      tcpClnt.IOHandler.Write(sUpd + CUSTOM_TERMINATOR);
      res := tcpClnt.IOHandler.ReadLn(CUSTOM_TERMINATOR);
      if trim(res) = 'done' then
      begin
        task.Comm.Send(MSG_SEND2CLIENTNEW_DONE, [sId, fClientId, ip]);
        verifySidAndDeleteUpdate(sId);
      end
{$IFDEF LOCALTEST}
      else if trim(res) = 'halted' then
      begin
        WriteToLog(sId + ' received halt-out-of-sequence', 'appp.log');
        raise Exception.Create('Halt out of sequence');
      end
{$ENDIF}
        ;
      tcpClnt.IOHandler.InputBuffer.Clear;
      tcpClnt.IOHandler.CloseGracefully;
      tcpClnt.Disconnect;
    except
      on e: Exception do
      begin
        if tcpClnt.Connected then
        begin
          tcpClnt.IOHandler.InputBuffer.Clear;
          tcpClnt.IOHandler.CloseGracefully;
        end;
        tcpClnt.Disconnect;
        task.Comm.Send(MSG_SEND2CLIENTNEW_EXCEPT, [fClientId, e.Message]);
        task.SetExitStatus(WORKER_EXIT_COMM_ERROR, fClientId); // 98 means error
        task.Terminate;
      end;
    end;
  finally
    tcpClnt.Free;
  end;
end;

procedure TSendWorker.OnWakeMessage(var msg: TOmniMessage);
var
  updatesToAsk: Integer;
begin
  // if populating, don't do anything
  if fAskedForUpdates then
    Exit;
  // process if updates present
  if (fUpdatesList.Count > 0) and (not fBusyInUpdate) then
  begin
    sendNextUpdate;
    Exit;
  end;
  // otherwise get next N updates
  // Now, we need to do a Communication with GUI to get this
  // more than one data item can be sent by enclosing in [] which can then
  // be accessed in receiver as msg.MsgData[0], 1, etc.
  updatesToAsk := WORKER_NUM_UPDATES_IN_MEMORY;
  if fClientInError then
    updatesToAsk := 1; // just try one update
  task.Comm.Send(MSG_GETCLIENTUPDATES, [fClientId, updatesToAsk]); // goes to oem2TaskMessage
  fAskedForUpdates := True;
end;

procedure TSendWorker.OnClientUpdatesResultMessage(var msg: TOmniMessage);
var
  UpdatesList: TList<TSendWorkerInfo>;
begin
  // we copy because the caller will delete object
  UpdatesList := TList<TSendWorkerInfo>(msg.MsgData);

  // if there are no updates, better end this task so that
  // next task from queue can start
  if UpdatesList.Count = 0 then
  begin
    UpdatesList.Free;
    task.SetExitStatus(WORKER_EXIT_NO_UPDATES_FOUND, fClientId);
    task.Terminate;
    Exit;
  end;

  fUpdatesList.Free;
  fUpdatesList := UpdatesList;
  fAskedForUpdates := False;
  fBusyInUpdate := False; // extra precaution
end;

procedure TfrmRepl.oem2TaskMessage(const task: IOmniTaskControl; const msg: TOmniMessage);
var
  aclient: string;
  updatesToAsk: Integer;
  UpdatesList: TList<TSendWorkerInfo>;
  SClient, sId, sEMsg, ip: string;
  AErrorID: Integer;
begin
  case msg.MsgID of
    MSG_GETCLIENTUPDATES:
      // must be processed by main thread for DB access
      begin
        aclient := msg.MsgData[0];
        updatesToAsk := msg.MsgData[1];
        UpdatesList := TList<TSendWorkerInfo>.Create;
        try
          WriteToLog(Format('Worker %s asking for %d updates from db', [aclient, updatesToAsk]), 'appp.log');
          getUpdatesForClient(aclient, updatesToAsk, UpdatesList);
          task.Comm.Send(MSG_CLIENTUPDATESRESULT, UpdatesList);
        finally
          // updatesList.Free; taking care of free in the worker
        end;
      end;
    MSG_SEND2CLIENTNEW_DONE:
      begin
        sId := msg.MsgData[0];
        SClient := msg.MsgData[1];
        ip := msg.MsgData[2];
        ExecSql(Format('delete from sync_msg where id=%s and comp=''%s''', [sId, SClient]));
        task.Comm.Send(MSG_RESETBUSYINUPDATE);
        WriteToLog(Format('Done id=%s comp=%s', [sId, SClient]), 'Done.log');
        LogPoolStatus;
      end;
    MSG_SEND2CLIENTNEW_EXCEPT:
      begin
        SClient := msg.MsgData[0];
        sEMsg := msg.MsgData[1];
        WriteToLog(Format('Exception (3) Send2Client %s (new code): ', [SClient]) + sEMsg, 'err.log');
        AErrorID := 0;
        if Pos('Socket Error # ', sEMsg) > 0 then
        begin
          try
            AErrorID := StrToInt(Copy(sEMsg, 16, 10));
          except
          end;
          if Assigned(CDReplicationService) then
            CDReplicationService.LogMessage(Format('Exception (3) Send2Client %s: %s', [SClient, sEMsg]), 1, 0,
              AErrorID);
        end;
        task.Comm.Send(MSG_RESETBUSYINUPDATE);
      end;
  end;
end;

procedure TSendWorker.OnResetBusyInUpdate(var msg: TOmniMessage);
begin
  fBusyInUpdate := False;
end;

procedure TfrmRepl.Button2Click(Sender: TObject);
var
  n: Integer;
  aWorkerTask: IOmniWorker;
const
  aclientId = 'MServer[192.168.0.150]';
begin
  for n := 0 to (FSendWorkerTaskList.Count - 1) do
  begin
    aWorkerTask := FSendWorkerTaskList[n];
    if TSendWorker(aWorkerTask.Implementor).isForClient(aclientId) then
    begin
      aWorkerTask.task.SetExitStatus(0, aclientId);
      aWorkerTask.task.Terminate;
      break;
    end;
  end;
end;

procedure TfrmRepl.oem2TaskTerminated(const task: IOmniTaskControl);
begin
  if task.ExitCode = WORKER_EXIT_LIST_DONE then
  begin
    WriteToLog
      (Format('Worker task %s ended after sending batch of updates. Queuing a new task after other tasks to look for updates again.',
      [task.Name]), 'appp.log');
    startWorker(task.Name, False);
    Exit;
  end;
  if task.ExitCode = WORKER_EXIT_NO_UPDATES_FOUND then
  begin
    WriteToLog
      (Format('Worker task %s ended as no updates in DB. Queuing a new task after other tasks to look for updates again.',
      [task.Name]), 'appp.log');
    startWorker(task.Name, False);
    Exit;
  end;
  if task.ExitCode = WORKER_EXIT_COMM_ERROR then
  begin
    // The only difference true makes here is that client is
    // retried but with very few updates to save memory
    WriteToLog
      (Format('Worker task %s ended due to error. Queuing a new   task after other tasks to check again after some time.',
      [task.Name]), 'appp.log');
    startWorker(task.Name, True);
    Exit;
  end;
  if task.ExitCode = WORKER_EXIT_REMOVE_COMMAND then
  begin
    WriteToLog(Format('Worker task %s ended as Remove Command received. Task is no longer queued.', [task.Name]),
      'appp.log');
    Exit;
  end;
  WriteToLog(Format('Worker task %s terminated with unknown exit code %d, %s', [task.Name, task.ExitCode,
    task.ExitMessage]), 'err.log');
  //next if added 06/22/2020 to restart after communication errors
  if task.ExitCode=0 then
    startWorker(task.Name, False);
end;

procedure TfrmRepl.oem2PoolThreadCreated(const pool: IOmniThreadPool; threadID: Integer);
begin
  { writeToLog(Format('Thread %d created in thread pool %d', [threadID,
    pool.uniqueId]), 'trc.log'); }
end;

procedure TfrmRepl.oem2PoolThreadDestroying(const pool: IOmniThreadPool; threadID: Integer);
begin
  { writeToLog(Format('Thread %d destroyed in thread pool %d', [threadID, pool.UniqueID])
    , 'trc.log'); }
end;

procedure TfrmRepl.oem2PoolThreadKilled(const pool: IOmniThreadPool; threadID: Integer);
begin
  { writeToLog(Format('Thread %d killed in thread pool %d', [threadID, pool.UniqueID])
    , 'trc.log'); }
end;

procedure TfrmRepl.LogPoolStatus;
begin
  WriteToLog(Format('Thread pool status: %d executing / %d queued', [GlobalOmniThreadPool.CountExecuting,
    GlobalOmniThreadPool.CountQueued]), 'appp.log');
end;

procedure TfrmRepl.oem2PoolWorkItemCompleted(const pool: IOmniThreadPool; taskID: Int64);
begin
  { writeToLog(Format('Task %d removed from pool', [taskID]), 'trc.log');
    if pool.IsIdle then
    WriteToLog(Format('Pool %d is empty', [pool.UniqueID]), 'trc.log'); }
end;

procedure TfrmRepl.getUpdatesForClient(aclient: string; numUpdates: Integer; slist: TList<TSendWorkerInfo>);
var
  sId, sExpr: string;
  sendInfo: TSendWorkerInfo;
const
  // The following is needed because TOP can not be parameterized
  sqlForProcess =
    'select top %d y.id, y.comp, x.Expr from sync x left join sync_msg y on y.id = x.id where y.id>0 and x.id>0 and y.comp = ''%s'' order by x.id;';
begin
  qryProcess.Sql.Text := Format(sqlForProcess, [numUpdates, aclient]);

  try
    if not qryProcess.Prepared then
      qryProcess.Prepare;

    qryProcess.Close;
    qryProcess.Open;

    qryProcess.First;
    while not qryProcess.EOF do
    begin
      sId := qryProcess.FieldByName('ID').AsString;
      sExpr := qryProcess.FieldByName('Expr').AsString;

      sendInfo.tpId := sId;
      sendInfo.tpUpd := sExpr;
      sendInfo.tpPort := ConnectPort;
      slist.Add(sendInfo);

      qryProcess.Next // ADone=1 indicates MAX_TASKS, retry same record rather than moving to next
    end;
  except
    on e: Exception do
      WriteToLog('Exception (4) : ' + e.Message + #13#10 + sExpr, 'err.log');
  end;
  qryProcess.Close;
end;

procedure TfrmRepl.HealthTimerTimer(Sender: TObject);
begin
  CheckForNewAllFiles;
end;

{$IFNDEF SYRCLIENT}

procedure TfrmRepl.getClientList(clientList: TStringList);
var
  sComp: string;
begin
  if tbSyncComp.Active then
    tbSyncComp.Close;
  tbSyncComp.Open;

  tbSyncComp.First;
  while not tbSyncComp.EOF do
  begin
    sComp := tbSyncComp.FieldByName('Comp').AsString;
    clientList.Add(sComp);
    tbSyncComp.Next;
  end;
  tbSyncComp.Close;
end;

procedure TfrmRepl.Timer1Timer(Sender: TObject);
begin
  if FileExists(ExtractFilePath(ParamStr(0))+'\Test.1') then
  begin
    DeleteFile(ExtractFilePath(ParamStr(0))+'\Test.1');
    Button2Click(nil);
  end;
end;

{$ENDIF}

initialization

fMutex := TMutex.Create(MUTEX_NAME);
{ if IsDesktopMode then
  begin
  Si.Connections := 'file(filename="CdSyncClient.sil", rotate="hourly")';
  Si.Enabled := True;
  end; }

finalization

fMutex.Free;

end.
