object frmRepl: TfrmRepl
  Left = 340
  Top = 251
  Caption = 'CompuDime Sync'
  ClientHeight = 513
  ClientWidth = 844
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poMainFormCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  TextHeight = 13
  object pnlBottom: TPanel
    Left = 0
    Top = 472
    Width = 844
    Height = 41
    Align = alBottom
    TabOrder = 1
    ExplicitTop = 471
    ExplicitWidth = 840
    object btnCreateTriggers: TButton
      Left = 195
      Top = 6
      Width = 137
      Height = 25
      Caption = 'Apply'
      TabOrder = 0
      OnClick = btnCreateTriggersClick
    end
    object Button2: TButton
      Left = 521
      Top = 3
      Width = 75
      Height = 33
      Caption = 'Button2'
      TabOrder = 1
      Visible = False
      OnClick = Button2Click
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 73
    Width = 844
    Height = 399
    Align = alClient
    TabOrder = 0
    ExplicitWidth = 840
    ExplicitHeight = 398
    object gbServer: TGroupBox
      Left = 1
      Top = 1
      Width = 842
      Height = 56
      Align = alTop
      Caption = 'Server'
      TabOrder = 0
      ExplicitWidth = 838
      object Label2: TLabel
        Left = 503
        Top = 27
        Width = 116
        Height = 13
        Caption = 'Port (default 8760/8761)'
      end
      object btnReg: TButton
        Left = 698
        Top = 17
        Width = 73
        Height = 23
        Caption = 'Register'
        TabOrder = 0
        OnClick = btnRegClick
      end
      object edtPort: TEdit
        Left = 623
        Top = 19
        Width = 67
        Height = 21
        TabOrder = 1
        TextHint = '8760'
      end
      object pnlServerSet: TPanel
        Left = 3
        Top = 15
        Width = 491
        Height = 33
        BevelOuter = bvNone
        ShowCaption = False
        TabOrder = 2
        object Label1: TLabel
          Left = 250
          Top = 12
          Width = 53
          Height = 13
          Caption = 'Register as'
        end
        object edClientName: TEdit
          Left = 309
          Top = 4
          Width = 177
          Height = 21
          TabOrder = 0
        end
        object edServer: TEdit
          Left = 3
          Top = 4
          Width = 241
          Height = 21
          TabOrder = 1
        end
      end
    end
    object gbtabs: TGroupBox
      Left = 1
      Top = 57
      Width = 842
      Height = 341
      Align = alClient
      Caption = 'Tables and Key Fields'
      TabOrder = 1
      ExplicitWidth = 838
      ExplicitHeight = 340
      object mTabs: TMemo
        Left = 2
        Top = 48
        Width = 838
        Height = 291
        Align = alClient
        Lines.Strings = (
          'AddOnPrd=UpcCode,AddOn'
          'Basket=Code'
          'Brand=Code'
          'CardSwip=Tel'
          'Category=Code'
          'CstmActn=Code'
          'CstmDspl=LineID'
          'CstmPrcs=PrdCode,CstCode'
          'CustCat=Cat'
          'CustInfo=Tel'
          'Customer=Tel'
          'DsplyReg=Code'
          'EnvRebat=;'
          'FsData=;'#9
          'InfoDes=Cat,Field'
          'Messages=Code'
          'MixHd=GroupID'
          'MixMatch=LineID'
          'PasRigt=;'
          'Password=Code'
          'PayList=LineID'
          'PrdInfo=UpcCode'
          'Prdmstr=Code'
          'PrdPrice=Guid'
          'RegKeys=ID'
          'Reports=Code'
          'RlList=ID'
          'SpecItem=Code'
          'ShipTo=LineID'
          'Synom=AutoNum'
          'WorkGrps=Code'
          'WorkRigt=Code')
        ScrollBars = ssVertical
        TabOrder = 1
        ExplicitWidth = 834
        ExplicitHeight = 290
      end
      object pnlServerMode: TPanel
        Left = 2
        Top = 15
        Width = 838
        Height = 33
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        ExplicitWidth = 834
        object lblMinutesBusy: TLabel
          Left = 192
          Top = 9
          Width = 190
          Height = 13
          Caption = 'Keep Clients Busy after Minutes Elapsed'
        end
        object chkAllowLocal: TCheckBox
          Left = 12
          Top = 5
          Width = 170
          Height = 17
          Caption = 'Run Server Local Mode (ALS)'
          TabOrder = 1
        end
        object seMinutesBusy: TSpinEdit
          Left = 394
          Top = 3
          Width = 42
          Height = 22
          MaxValue = 0
          MinValue = 0
          TabOrder = 0
          Value = 15
        end
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 844
    Height = 73
    Align = alTop
    TabOrder = 2
    ExplicitWidth = 840
    object GroupBox1: TGroupBox
      Left = 640
      Top = 1
      Width = 203
      Height = 71
      Align = alRight
      Caption = 'Custom Service Name (suffix)'
      TabOrder = 0
      ExplicitLeft = 636
      object btnCustomName: TButton
        Left = 136
        Top = 17
        Width = 60
        Height = 25
        Caption = 'Set'
        TabOrder = 0
        OnClick = btnCustomNameClick
      end
      object edtCustomName: TEdit
        Left = 9
        Top = 19
        Width = 121
        Height = 21
        TabOrder = 1
        TextHint = 'Suffix to Service Name'
      end
    end
    object grpDbConnectionSet: TGroupBox
      Left = 1
      Top = 1
      Width = 639
      Height = 71
      Align = alClient
      Caption = 'Database Connection Settings'
      TabOrder = 1
      ExplicitWidth = 635
      object edCon: TEdit
        Left = 14
        Top = 20
        Width = 411
        Height = 21
        ReadOnly = True
        TabOrder = 2
      end
      object btnSetCon: TButton
        Left = 431
        Top = 18
        Width = 84
        Height = 23
        Caption = 'Set Connection'
        TabOrder = 0
        OnClick = btnSetConClick
      end
      object Button1: TButton
        Left = 520
        Top = 18
        Width = 75
        Height = 23
        Caption = 'Start'
        TabOrder = 1
        OnClick = Button1Click
      end
    end
  end
  object qryExec: TAdsQuery
    DatabaseName = 'con'
    AdsConnection = con
    Left = 392
    Top = 159
    ParamData = <>
  end
  object con: TAdsConnection
    AdsServerTypes = [stADS_REMOTE, stADS_LOCAL]
    LoginPrompt = False
    Username = 'ADSSYS'
    AfterConnect = conAfterConnect
    Left = 152
    Top = 215
  end
  object oDlg: TOpenDialog
    DefaultExt = '*.add'
    Filter = '*.add|*.add'
    Options = [ofHideReadOnly, ofFileMustExist, ofShareAware, ofEnableSizing]
    Title = 'Please select Advantage Server Database file'
    Left = 304
    Top = 215
  end
  object client: TIdTCPClient
    ConnectTimeout = 0
    Port = 8760
    ReadTimeout = 0
    Left = 497
    Top = 162
  end
  object serv: TIdTCPServer
    Bindings = <>
    DefaultPort = 8760
    OnDisconnect = servDisconnect
    OnException = servException
    OnListenException = servListenException
    OnExecute = servExecute
    Left = 496
    Top = 218
  end
  object servTimer: TTimer
    Enabled = False
    Interval = 100
    OnTimer = servTimerTimer
    Left = 281
    Top = 281
  end
  object tbSyncMsg: TAdsTable
    IndexName = 'IDX_SYNC_MSG_ID'
    StoreActive = False
    AdsConnection = con
    TableName = 'Sync_Msg'
    Left = 176
    Top = 160
  end
  object qrySyncQuery: TAdsQuery
    SQL.Strings = (
      'Select * From Sync Where ID=:MsgID;')
    AdsConnection = con
    Left = 248
    Top = 160
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'MsgID'
        ParamType = ptUnknown
      end>
  end
  object qryCheckTime: TAdsQuery
    SQL.Strings = (
      
        'Select s.*, TimeStampDiff(SQL_TSI_MINUTE, s.Stamp, CurTime()) Di' +
        'ff From SyncTime s '
      'Where s.Created=CurDate();')
    AdsConnection = con
    Left = 320
    Top = 160
    ParamData = <>
  end
  object ClientBusy: TTimer
    Enabled = False
    Interval = 600000
    OnTimer = ClientBusyTimer
    Left = 376
    Top = 272
  end
  object oem1: TOmniEventMonitor
    OnTaskMessage = oem1TaskMessage
    OnTaskTerminated = oem1TaskTerminated
    Left = 257
    Top = 360
  end
  object IdAntiFreeze1: TIdAntiFreeze
    Left = 497
    Top = 288
  end
  object qryProcess: TAdsQuery
    SQL.Strings = (
      
        'select top :NumUpdates y.id, y.comp, x.Expr from sync x left joi' +
        'n sync_msg y on y.id = x.id where y.comp not in (select * from s' +
        'ync_down) order by y.id;')
    AdsConnection = con
    Left = 568
    Top = 168
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'NumUpdates'
        ParamType = ptUnknown
      end>
  end
  object tbSyncComp: TAdsTable
    StoreActive = False
    AdsConnection = con
    TableName = 'Sync_Comp'
    Left = 640
    Top = 168
  end
  object cleanupDownlistTimer: TTimer
    Enabled = False
    Interval = 600000
    OnTimer = cleanupDownlistTimerTimer
    Left = 576
    Top = 224
  end
  object oem2: TOmniEventMonitor
    OnPoolThreadCreated = oem2PoolThreadCreated
    OnPoolThreadDestroying = oem2PoolThreadDestroying
    OnPoolThreadKilled = oem2PoolThreadKilled
    OnPoolWorkItemCompleted = oem2PoolWorkItemCompleted
    OnTaskMessage = oem2TaskMessage
    OnTaskTerminated = oem2TaskTerminated
    Left = 321
    Top = 360
  end
  object HealthTimer: TTimer
    Interval = 120000
    OnTimer = HealthTimerTimer
    Left = 640
    Top = 224
  end
end
