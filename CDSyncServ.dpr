program CDSyncServ;

{$R *.dres}

uses
  Forms,
  Windows,
  Dialogs,
  SvcMgr,
  SysUtils,
  Unit1 in 'Unit1.pas' {frmRepl},
  fileswork in 'Modules\fileswork.pas',
  WININFO in 'Modules\WININFO.PAS',
  utils in 'Modules\utils.pas',
  log in 'Modules\log.pas',
  optimize in 'Modules\optimize.pas',
  mutex in 'Modules\mutex.pas',
  RegAsService in 'RegAsService.pas' {CDReplicationService: TService},
  Unit2 in 'Unit2.pas' {PasswordDlg};

{$R *.res}

begin
  if not IsDesktopMode then
  begin
    SvcMgr.Application.Initialize;
    SvcMgr.Application.Title := 'CompuDime Sync';
    Application.CreateForm(TCDReplicationService, CDReplicationService);
    Forms.Application.CreateForm(TfrmRepl, frmRepl);
    frmRepl.ListenServer;
    if not (SameText(ParamStr(1), '/install') or SameText(ParamStr(1), '/uninstall')) then
      frmRepl.StartServTimer;
    SvcMgr.Application.Run;
  end
  else
  begin
    Forms.Application.Initialize;
    Forms.Application.CreateForm(TfrmRepl, frmRepl);
    Forms.Application.CreateForm(TPasswordDlg, PasswordDlg);
    if (ParamStr(1) = '/auto') then
    begin
      Forms.Application.ShowMainForm := False;
      frmRepl.ListenServer;
      frmRepl.StartServTimer;
    end;
    Forms.Application.Run;
  end;

end.
