unit mutex;

interface

uses Windows, Classes, SysUtils, SyncObjs, DateUtils;

type
  TMutex = class
  public
    hMutex: THandle;
    constructor Create(Name: String; flRaiseIfExist: boolean = false);
    destructor Destroy; override;
    function Enter(maxTime: dword = INFINITE): boolean;
    procedure Leave;
  end;

implementation

{ TMutex }

constructor TMutex.Create(Name: String; flRaiseIfExist: boolean = false);
var
  Err: integer;
  Sa: TSecurityAttributes;
  Sd: TSecurityDescriptor;
begin
  InitializeSecurityDescriptor(@Sd, SECURITY_DESCRIPTOR_REVISION);
  SetSecurityDescriptorDacl(@Sd, true, nil, false);
  Sa.nLength := SizeOf(Sa);
  Sa.lpSecurityDescriptor := @Sd;
  Sa.bInheritHandle := true;

  hMutex := CreateMutex(@Sa, false, PChar(Name));
  Err := GetLastError();
  if (Err = ERROR_ALREADY_EXISTS) and flRaiseIfExist then
    raise Exception.Create('already exist');
  if (Err = ERROR_ALREADY_EXISTS) or (Err = ERROR_ACCESS_DENIED) then
  begin
    hMutex := OpenMutex(MUTEX_ALL_ACCESS, false, PChar(Name));
    Win32Check(hMutex <> 0);
  end;
end;

function TMutex.Enter(maxTime: dword = INFINITE): boolean;
begin
  Result := WaitForSingleObject(hMutex, maxTime) = WAIT_OBJECT_0;
  // Inc(CallCount);
end;

procedure TMutex.Leave;
begin
  // Dec(CallCount);
  Win32Check(ReleaseMutex(hMutex));
end;

destructor TMutex.Destroy;
begin
  // CloseHandle(hMutex);
end;

initialization

finalization

end.
