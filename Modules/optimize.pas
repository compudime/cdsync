unit optimize;

interface

uses Windows, Classes, SysUtils, SyncObjs;

var
  CriticalSectionsList: TStringList;

type
  TOptimizeCriticalSection = class(TCriticalSection)
  public
    Name: String;
    CallCount: integer;
    EnterTime: Comp;
    TotalTime: Double;
    flSave2Log: boolean;
    constructor Create(SectionName: String; flSaveLog: boolean = true);
    procedure Acquire; override;
    procedure Release; override;
    destructor Destroy; override;
  end;

implementation

{ TOptimizeCriticalSection }

constructor TOptimizeCriticalSection.Create(SectionName: String; flSaveLog: boolean = true);
begin
  inherited Create;
  CallCount := 0;
  TotalTime := 0;
  Name := SectionName;
  flSave2Log := flSaveLog;
end;

procedure TOptimizeCriticalSection.Acquire;
begin
  inherited;
  if FSection.RecursionCount = 1 then
    EnterTime := GetTickCount(); // TimeStampToMSecs(DateTimeToTimeStamp(Now()));
end;

procedure TOptimizeCriticalSection.Release;
begin
  Inc(CallCount);
  if FSection.RecursionCount = 1 then
    TotalTime := TotalTime + GetTickCount() - EnterTime;
  inherited;
end;

destructor TOptimizeCriticalSection.Destroy;
var
  Info: String;
begin
  Info := Format('TotalTime : %f; TotalCall : %d;', [TotalTime / 1000, CallCount]);
  { CriticalSectionsList.IndexOfName(Name)
    if >=0 then
    CriticalSectionsList.Values[Name]:=Info
    else }
  if flSave2Log then
    CriticalSectionsList.Add(Name + '=' + Info);
  inherited;
end;

initialization

CriticalSectionsList := TStringList.Create;

finalization

if Assigned(CriticalSectionsList) and (CriticalSectionsList.Count > 0) then
begin
  CriticalSectionsList.SaveToFile(ExtractFilePath(ParamStr(0)) + 'optimize_info.txt');
  CriticalSectionsList.Free;
end;

end.
