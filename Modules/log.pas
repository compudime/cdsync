unit log;

interface

uses Windows, Classes, SysUtils, Optimize;

var
  Log_Lock: TOptimizeCriticalSection;
  CurModuleFileName: array [0 .. 1024] of char;
  CurModulePath: string;
  flAddDateTime2Log: boolean;

procedure WriteToLog(const sData: string; LogFileName: String = ''; flNoLock: boolean = False);

implementation

uses
  System.SyncObjs, System.IOUtils;

procedure DeleteFilesOlderThan(const Days: Integer; const Path: string;
  const SearchPattern: string = '*.*');
var
  FileName: string;
  OlderThan: TDateTime;
begin
  Assert(Days >= 0);
  OlderThan := Now() - Days;
  for FileName in TDirectory.GetFiles(Path, SearchPattern) do
    if TFile.GetCreationTime(FileName) < OlderThan then
      TFile.Delete(FileName);
end;

procedure WriteToLog(const sData: string; LogFileName: String; flNoLock: boolean);
var
  fsLog: TFileStream;
  sFullPath, sTemp: string;
  dtAddTime: TDateTime;
  ARawByteString: AnsiString;
begin
  if not SameText(LogFileName, 'err.log') then
    Exit;
  /// ////////////////////////////////////////Disable other logs, for size, speed and memory

  if (Length(sData) > 0) then
  begin
    if SameText(LogFileName, 'err.log') then
      LogFileName := '';

    if LogFileName = '' then
      LogFileName := 'App-' + FormatDateTime('yyyy-mm-dd', Now()) + '.log';

    sTemp := '';
    if flAddDateTime2Log then
    begin
      dtAddTime := Now;
      sTemp := FormatDateTime('dd.mm.yyyy" "HH:NN:SS', dtAddTime) + ';';
    end;

    sTemp := sTemp + sData + #13#10;
    ARawByteString := AnsiString(sTemp);
    if not flNoLock then
      Log_Lock.Enter;
    try
      sFullPath := CurModulePath + LogFileName;
      fsLog := nil;
      if FileExists(sFullPath) then
      begin
        fsLog := TFileStream.Create(sFullPath, (fmOpenReadWrite or fmShareDenyWrite));
        { if fsLog.Size>250000 then
          begin
          if FileExists(ChangeFileExt(sFullPath, '.bak')) then
          DeleteFile(ChangeFileExt(sFullPath, '.bak'));
          fsLog.Free;
          fsLog := nil;
          RenameFile(sFullPath, ChangeFileExt(sFullPath, '.bak'));
          end; }
      end;
      if not Assigned(fsLog) then
      begin
        DeleteFilesOlderThan(10, CurModulePath, 'App-*.log');
        fsLog := TFileStream.Create(sFullPath, fmCreate);
      end;

      try
        fsLog.Seek(0, soFromEnd);
        fsLog.Write(ARawByteString[1], Length(ARawByteString));
      finally
        fsLog.Free;
      end;
    finally
      if not flNoLock then
        Log_Lock.Leave;
    end;
  end;
end;

initialization

flAddDateTime2Log := True;
Log_Lock := TOptimizeCriticalSection.Create('Log_Lock', False);
if GetModuleFileName(GetModuleHandle(nil), CurModuleFileName, 1024) = 0 then
  RaiseLastOSError;
CurModulePath := ExtractFilePath(CurModuleFileName);

finalization

Log_Lock.Free;

end.
