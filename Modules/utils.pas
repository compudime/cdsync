unit utils;

interface

uses Windows, Classes, SysUtils, SyncObjs, DateUtils;

function CheckExpired(dt: TDateTime = 0): Boolean;
function StrToWide(p_sSource: string): PWideChar;
function StringBetween(const S: AnsiString; const Start, Stop: AnsiString): AnsiString;

implementation

uses StrUtils;

function StrToWide(p_sSource: string): PWideChar;
var
  wsBuffer: array [0 .. 1024] of widechar;
begin
  // Result:=p_sSource;
  result := StringToWideChar(p_sSource, wsBuffer, Length(p_sSource) + 1);
end;

function CheckExpired(dt: TDateTime = 0): Boolean;
// var
// CheckTime : TIdTime;
begin
  // if dt=0 then
  // GetModif
  // CheckTime:=TIdTime.Create(nil);
  // CheckTime.Host:='time.windows.com';
  result := DaysBetween(Date, dt) > 100;
end;

// kamosion Now Start and Stop - String (char before)
function StringBetween(const S: AnsiString; const Start, Stop: AnsiString): AnsiString;
var
  PosStart, PosEnd: Integer;
begin
  PosStart := Pos(Start, S);
  PosEnd := PosEx(Stop, S, PosStart + Length(Start)); // PosEnd has to be after PosStart.

  if (PosStart > 0) and (PosEnd > PosStart) then
    result := Copy(S, PosStart + Length(Start), PosEnd - PosStart - Length(Start))
  else
    result := '';
end;

end.
