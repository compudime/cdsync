program CDSyncClient;

uses
  Forms,
  SvcMgr,
  SysUtils,
  Unit1 in 'Unit1.pas' {frmRepl} ,
  RegAsService in 'RegAsService.pas' {SybaseReplicationClientService: TService} ,
  Unit2 in 'Unit2.pas' {PasswordDlg} ,
  Unit3SendMail in 'Unit3SendMail.pas' {dmSendEmail: TDataModule};

{$R *.res}

begin
  if not IsDesktopMode then
  begin
    SvcMgr.Application.Initialize;
    SvcMgr.Application.CreateForm(TCDReplicationService, CDReplicationService);
    Application.CreateForm(TdmSendEmail, dmSendEmail);
    Forms.Application.CreateForm(TfrmRepl, frmRepl);
    if not((ParamStr(1) = '/install') or (ParamStr(1) = '/uninstall')) then
    begin
      frmRepl.ListenServer;
      frmRepl.StartServTimer;
    end;
    SvcMgr.Application.Run;
  end
  else
  begin
    Forms.Application.Initialize;
    Forms.Application.CreateForm(TfrmRepl, frmRepl);
    Forms.Application.CreateForm(TPasswordDlg, PasswordDlg);
    if (ParamStr(1) = '/auto') then
    begin
      Forms.Application.ShowMainForm := False;
      frmRepl.ListenServer;
      frmRepl.StartServTimer;
    end;
    Forms.Application.Run;
  end;

end.
